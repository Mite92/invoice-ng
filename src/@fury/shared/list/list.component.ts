import { AfterViewInit, Component, ElementRef, EventEmitter, Input, Output, ViewChild, ViewEncapsulation } from '@angular/core';
import { fromEvent } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
import { ListColumn } from './list-column.model';
import { log } from 'util';
import * as moment from "moment"


export enum type {
  text = 1,
  date,
  dateFromTo,
}

@Component({
  selector: 'fury-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  encapsulation: ViewEncapsulation.None
})

export class ListComponent implements AfterViewInit {

  @Input() name: string;
  @Input() columns: ListColumn[];

  @ViewChild('filter', { static: false }) filter: ElementRef;
  @ViewChild('filter2', { static: false }) filter2: ElementRef;
  @Output() filterChange = new EventEmitter<string>();
  @Output() filterSubmit = new EventEmitter<string | object>();

  @Input() hideHeader: boolean;

  @Input() type: type = 1;

  constructor() {
  }

  emitEvent() {
    this.filterSubmit.emit(this.filter.nativeElement.value);
  }

  emitEventObject() {
    this.filterSubmit.emit(
      {
        dateStart: (this.filter.nativeElement.value != "") ? moment(this.filter.nativeElement.value) : undefined,
        dateEnd: (this.filter2.nativeElement.value != "") ? moment(this.filter2.nativeElement.value) : undefined
      }
    );
  }

  ngAfterViewInit() {
    if (!this.hideHeader) {
      fromEvent(this.filter.nativeElement, 'keyup').pipe(
        distinctUntilChanged(),
        debounceTime(150)
      ).subscribe(() => {      
          // Enter pressed
          this.filterChange.emit(this.filter.nativeElement.value);       
      });
    }
  }

  toggleColumnVisibility(column, event) {
    event.stopPropagation();
    event.stopImmediatePropagation();
    column.visible = !column.visible;
  }
}
