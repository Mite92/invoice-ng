import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '../material-components.module';
import { ListComponent } from './list.component';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    MaterialModule,
    FormsModule,
    SharedModule
  ],
  declarations: [ListComponent],
  exports: [ListComponent]
})
export class ListModule {
}
