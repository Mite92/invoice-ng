import { Component, Inject, Renderer2, Injector } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { SidenavService } from '@app/layout/sidenav/sidenav.service';
import { MatIconRegistry, MatDialog } from '@angular/material';
import { ThemeService } from '@fury/services/theme.service';
import { Platform } from '@angular/cdk/platform';
import { ActivatedRoute } from '@angular/router';
import { filter } from 'rxjs/operators';
import { AppComponentBase } from '@shared/app-component-base';

// declare var rootComponent: any;
@Component({
    selector: 'fury-root',
    templateUrl: './root.component.html'
})
export class RootComponent extends AppComponentBase {

    constructor(
        private injector: Injector,
        private sidenavService: SidenavService,
        private iconRegistry: MatIconRegistry,
        private renderer: Renderer2,
        private themeService: ThemeService,
        @Inject(DOCUMENT) private document: Document,
        private platform: Platform,
        private route: ActivatedRoute,
        private _dialog: MatDialog,
    ) {
        super(injector);

        this.route.queryParamMap.pipe(
            filter(queryParamMap => queryParamMap.has('style'))
        ).subscribe(queryParamMap => this.themeService.setStyle(queryParamMap.get('style')));

        this.themeService.setNavigation("top");
        this.themeService.setToolbarVisible(true);

        this.iconRegistry.setDefaultFontSetClass('material-icons');
        this.themeService.theme$.subscribe(theme => {
            if (theme[0]) {
                this.renderer.removeClass(this.document.body, theme[0]);
            }

            this.renderer.addClass(this.document.body, theme[1]);
        });

        if (this.platform.BLINK) {
            this.renderer.addClass(this.document.body, 'is-blink');
        }

        this.sidenavService.addItems([
            {
                name: 'Dashboard',
                routeOrFunction: '/',
                icon: 'dashboard',
                pathMatchExact: true
            },
            {
                name: this.l('Users'),
                routeOrFunction: '/users',
                icon: 'people',
                // permission: 'Pages.Users'
            },
            {
                name: this.l('Roles'),
                routeOrFunction: '/roles',
                icon: 'local_offer',
                // permission: 'Pages.Roles'
            },
            {
                name: 'Registers',
                icon: 'description',
                subItems: [
                    {
                        name: 'Products',
                        routeOrFunction: "/registers/products"
                    },
                    {
                        name: 'Customers',
                        routeOrFunction: '/registers/customers',
                    },
                    {
                        name: 'Master Keys',
                        routeOrFunction: '/registers/master-keys',
                    },
                    {
                        name: 'Master Key Values',
                        routeOrFunction: '/registers/master-key-values',
                    }
                ]
            },
            {
                name: 'Documents',
                icon: 'description',
                subItems: [
                    {
                        name: 'Invoices',
                        routeOrFunction: '/documents/invoices',
                    },
                    {
                        name: 'Invoice Status',
                        routeOrFunction: '/documents/payments',
                    },
                    {
                        name: 'Purchases',
                        routeOrFunction: '/documents/purchases',
                    }
                ]
            },
        ]);
    }
}
