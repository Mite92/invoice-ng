import { Component, OnInit, Output, EventEmitter, Input, ViewChild, ElementRef, OnChanges } from '@angular/core';
import { FormControl } from '@angular/forms';
import escape from 'lodash-es/escape';
import { Observable } from 'rxjs';

@Component({
	selector: 'custom-autocomplete1',
	templateUrl: './autocomplete.component.html',
	styleUrls: ['./autocomplete.component.scss']
})
export class CustomAutoCompleteComponent implements OnInit, OnChanges {

	@Input() properties: any;
	@Input() model: any;

	dataResult: any;

	@Output() searchInput = new EventEmitter<string>();
	@Output() selectedId = new EventEmitter<number>();



	readonly: boolean = false;

	@ViewChild('filter', { static: false }) filter: ElementRef;

	autocompleteCtrl: FormControl;
	matAutocomplete: FormControl;

	constructor() {
		this.getData = this.getData.bind(this);
	}

	ngOnInit() {
		this.autocompleteCtrl = new FormControl();
		this.getData(null);
	}

	getData(search) {

		let input = {
			maxResultCount: 999,
			displayName: search

		};

		this.properties.service.search(input).subscribe((result) => {
			this.dataResult = result.items;

			// if (this.properties.defaultValue != undefined || this.properties.defaultValue != null) {
			// 	this.autocompleteCtrl.setValue(this.properties.defaultValue.displayName);
			// 	this.readonly = true;
			// 	this.properties.defaultValue = undefined;
			// }
		});

	}

	searchEvent() {

		let value = this.filter.nativeElement.value.toLowerCase();

		if (value == undefined || value.length == 0 || value.length > 2) {
			if (!this.readonly) {
				this.getData(value);
			}
		}
	}

	optionSelected(value) {
		debugger;
		this.readonly = true;

		if (this.properties.onItemSelected) {
			this.selectedId.emit(value.option.id);
			// this.properties.onItemSelected(value.option.id);
		}
	}

	reset() {
		this.autocompleteCtrl.reset();
		this.readonly = false;
		this.searchInput.emit();
		this.selectedId.emit(null)
	}

	ngOnChanges() {
		if (this.model)
			this.autocompleteCtrl.setValue(this.model);
		else
			this.autocompleteCtrl.setValue(null);
	}

}
