import { Injectable } from '@angular/core';

@Injectable()
export class QuickAddService {

    id: number;
    name: string;

    setId(id: number) {
        this.id = id
    }

    getId(): number {
        return this.id;
    }

    setName(name: string) {
        this.name = name;
    }

    getName(): string {
        return this.name;
    }

}