import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LayoutComponent } from '@app/layout/layout.component';
import { AppRouteGuard } from '@shared/auth/auth-route-guard';

const routes: Routes = [
    // { path: '', redirectTo: '/app', pathMatch: 'full' },
    {
        path: 'login',
        loadChildren: './app/pages/authentication/login/login.module#LoginModule'
    },
    {
        path: 'register',
        loadChildren: './app/pages/authentication/register/register.module#RegisterModule'
    },
    {
        path: '',
        component: LayoutComponent,
        children: [
            {
                path: '',
                loadChildren: './app/pages/dashboard/dashboard.module#DashboardModule',
                pathMatch: 'full',
                canActivate: [AppRouteGuard]
            },
            {
                path: 'profile',
                loadChildren: './app/pages/profile/profile.module#ProfileModule',
                canActivate: [AppRouteGuard]
            },
            {
                path: 'roles',
                loadChildren: './app/pages/roles/roles.module#RolesModule',
                data: { permission: 'Pages.Roles' },
                canActivate: [AppRouteGuard]

            },
            {
                path: 'users',
                loadChildren: './app/pages/users/users.module#UsersModule',
                data: { permission: 'Pages.Users' },
                canActivate: [AppRouteGuard]
            },
            {
                path: "registers",
                loadChildren: './app/pages/register/registers.module#RegistersModule',
                // runGuardsAndResolvers: "always",
                // canActivate: [AppRouteGuard],
            },
            {
                path: "documents",
                loadChildren: './app/pages/documents/documents.module#DocumentsModule',
                // runGuardsAndResolvers: "always",
                // canActivate: [AppRouteGuard],
            },
        ]
    },
    { path: '**', redirectTo: '', pathMatch: 'full' },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule],
    providers: []
})
export class RootRoutingModule { }
