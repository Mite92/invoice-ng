import { PipeTransform, Pipe, Output, EventEmitter } from "@angular/core";

@Pipe({
    name: "filter"
})
export class FilterPipe implements PipeTransform {

    @Output() itemsCount: EventEmitter<any> = new EventEmitter();

    transform(item: any[], searchTerm: string): any[] {
        if (!item || !searchTerm) {
            return item;
        }

        let count = item.filter(it => it.displayName.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1).length;

        this.itemsCount.emit(count);

        return item.filter(it => it.displayName.toLowerCase().indexOf(searchTerm.toLowerCase()) !== -1
        );
    }
}
