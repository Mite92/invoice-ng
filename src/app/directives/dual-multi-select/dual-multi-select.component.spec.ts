import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DualMultiSelectComponent } from './dual-multi-select.component';

describe('DualMultiSelectComponent', () => {
  let component: DualMultiSelectComponent;
  let fixture: ComponentFixture<DualMultiSelectComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DualMultiSelectComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DualMultiSelectComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
