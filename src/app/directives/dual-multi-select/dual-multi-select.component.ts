import { Component, OnInit, Input, OnChanges } from '@angular/core';
import * as _ from 'lodash';
import { FormArray } from '@angular/forms';
import { CdkDragDrop, moveItemInArray, transferArrayItem } from '@angular/cdk/drag-drop';
import { timeout } from 'rxjs/operators';

@Component({
	selector: 'app-dual-multi-select',
	templateUrl: './dual-multi-select.component.html',
	styleUrls: ['./dual-multi-select.component.scss']
})
export class DualMultiSelectComponent implements OnInit, OnChanges {

	@Input() data: any;
	@Input() options: any;
	@Input() model: any;

	selectedItems: any = [];
	searchTerm: string = "";

	constructor() { }

	ngOnInit() {
		console.log('data', this.data);
		console.log('options', this.options);
	}

	reset() {

		this.selectedItems = [];

		this.model.forEach((el) => {

			let item = _.find(this.data, (obj) => {
				return obj.id == el;
			});
			if (item) {
				this.selectedItems.push(item);
			}
		});
	}

	add() {
		this.data.forEach((el) => {
			if (el.selected) {
				el.selected = false;
				this.selectedItems.push(el);
			}
		});
		this.updateModel();
	}

	addAll() {
		this.selectedItems = [];


		this.data.forEach((el) => {
			el.selected = false;
			this.selectedItems.push(el);

		});
		this.updateModel();
	}

	removeAll() {
		this.selectedItems = [];
		this.updateModel();
	}

	remove() {
		let removeElements = [];


		this.selectedItems.forEach((el) => {
			if (el.selected) {
				removeElements.push(el);
				el.selected = false;
			}
		});

		this.selectedItems = _.filter(this.selectedItems, (el) => {
			return removeElements.indexOf(el) == -1;
		});

		this.updateModel();
	}

	selectItem(item) {
		item.selected = !item.selected;
	}

	alreadyAdded(item) {

		let idx = this.selectedItems.findIndex((obj) => {
			return obj.id == item.id;
		});

		if (idx > -1) {
			return "none";
		}
		else {
			return "block";
		}
		// return idx > -1;
	}

	sortableUpdate(event, ui) {

		setTimeout(() => {
			this.updateModel();
			console.log(this.selectedItems);

		}, 200);
	}

	updateModel() {

		this.model.splice(0, this.model.length);
		this.selectedItems.forEach((el) => {
			this.model.push(el.id);
		});
		console.log(this.model);
	}

	ngOnChanges() {

		this.reset();
	}

}
