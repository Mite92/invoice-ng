import { Directive, ElementRef, Input, OnInit, Output, EventEmitter, Renderer2, HostListener } from '@angular/core';
/*tslint:disable*/
import {
  AfterViewInit, ViewChild, OnChanges
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
// import { Http, Response } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { NgControl, NgModel } from '@angular/forms';


@Directive({
  selector: '[ajax-options]'
})
export class AjaxOptionsDirective implements OnInit {

  @Input('ajax-options') ajaxOptions: any = {};
  @Input('propName') colValue: any;
  @Input() functions: any;

  @Output() emmitedItem: EventEmitter<any> = new EventEmitter();
  @Output() emmitedAjaxId: EventEmitter<any> = new EventEmitter();

  items: any[];

  options = {
    language: 'mk',
    allowClear: true,
    placeholder: "-",

  };

  data: any = {};

  ajaxNavisionList: any = [];

  constructor(
    public element: ElementRef,
    public renderer: Renderer2
  ) { }


  @HostListener('mouseenter') onMouseEnter() {
    // alert("a");
  }

  selectElement = $(this.element.nativeElement);

  ngOnInit() {

    let x = this;

    let isOpen = false;

    this.options = Object.assign(this.options, this.ajaxOptions);

    if (this.ajaxOptions) {

      let ajax = this.ajaxOptions.ajax;
      let urlSplit = ajax.url.split('/');
      urlSplit[urlSplit.length - 1] = 'getAll';

      let url = urlSplit.join('/');
    }

    setTimeout(() => {

      // this.selectElement.select2(this.options);

      this.selectElement.on('select2:open', function () {
        isOpen = true;
      })

      this.selectElement.on('select2:close', function () {
        isOpen = false;
      })

      this.selectElement.on('select2:select', function (e) {
        debugger;
        let value = $(this).val();

        //  x.data = e.params.data;
        x.data.value = 99;
        x.data.propName = x.colValue;
        x.emmitedItem.emit(x.data);
        // x.emmitedAjaxId.emit(value);
        x.renderer.setAttribute(x.element.nativeElement, 'value', value.toString());
        //$('#' + elementId).val(null);
      })

      this.selectElement.on('select2:unselect', function (e) {
        // x.data = e.params.data;
        x.data.id = null;
        x.data.propName = x.colValue;
        //x.emmitedItem.emit(x.data);
      })
    });


    if (this.functions) {
      this.functions.setOptions = x.setOptions;
      this.functions.recreate = x.recreateSelect;
      this.functions.getSelectedText = x.getSelectedText;
      this.functions.getSelectedNavision = x.getSelectedNavision;
      this.functions.getSelectedValue = x.getSelectedValue;
    }
  }

  refreshSelect() {

    debugger;
    // if (!$('.select2-ajax').select2Initialized) return;
    setTimeout(() => {
      $('.select2-ajax').trigger('change');
    });


  }

  setOptions(options: any) { //each option must be in this format: {value:, text:}

    debugger;

    let content = "";
    let elementId = "";

    options.forEach(el => {

      if (el.value && el.text && el.elementId) {
        content += "<option value='" + el.value + "'>" + el.text + "</option>";
        elementId += el.elementId;
      }
      else {
        elementId += el.elementId;
        content += "";
        $('#' + elementId).val(null);
      }
    });

    $('#' + elementId).html(content);
    setTimeout(() => {
      $('#' + elementId).trigger('change');
    });
  }

  recreateSelect = function () {
    setTimeout(() => {
      this.selectElement.select2('destroy');
      this.selectElement.select2(this.options);
    });
  };

  getSelectedText(id) {
    // ovoj metod se koristi za displayColum value vo master detail tabelata
    // return $('#' + id).find('option:selected').text();
    return $('#' + id).val();

  }

  getSelectedValue(id) {
    return $('#' + id).attr("value");
  }

  getSelectedNavision() {

    debugger;
    // let elemText = this.selectElement.find('option:selected').text();
    // let elemBarcode = this.ajaxNavisionList.find(z => z.selected);
    // let selection = {
    //     text: elemText,
    //     barcode: elemBarcode ? elemBarcode.barcode : null
    // };
    // return selection;
  }
}
