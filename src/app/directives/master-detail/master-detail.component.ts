/* tslint:disable */
import {
	Component,
	OnInit,
	Input,
	EventEmitter,
	Output,
	ElementRef,
	NgModuleFactoryLoader,
	ViewChild,
	Renderer2
} from "@angular/core";
import * as moment from "moment";
import { finalize, catchError, startWith, map } from "rxjs/operators";
import * as _ from "lodash";
import { Router } from "@angular/router";

import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';

@Component({
	selector: "master-detail",
	templateUrl: "./master-detail.component.html",
	styleUrls: ["./master-detail.component.scss"]
})
export class MasterDetailComponent implements OnInit {

	// Usage:
	//     <masterDetail></masterDetail>
	// properties:
	// title - title of the box
	// link - useful for giving a link if a value is pulled from somewhere
	// linkName - how the link is shown on UI
	// model - master record, should have Id at any point
	// masterColumn - column name of the master record foreign key
	// service - service object for handling the master detail
	// height - height of the display table
	// btnsClass - class to be added to the buttons  wrapper
	// onBeforeCreate - function that executes before creation of the detail record
	// onAfterCreate - function that executes after creation of the detail record
	// onBeforeUpdate - function that executes before updating of the detail record
	// onNew - function that executes before updating of the detail record
	// onBeforeGet - function that executes before updating of the detail record
	// inputs:
	// type - input, select, checkbox...
	// label - label of the input
	// column - column for the input in database
	// displayColumn - column for the dispaly value of the input
	// barcode - column for the barcode if type == 'navision'
	// serial - enter which column is for serial if type == 'navision'
	// dropDownName - name of he list from dbo.DropDown to get the list values
	// options - options of the dropdown list
	// getSelectOptions - function which gets dropdown list options if ddlName is not given
	// selectAjaxOptions - ajax options for select2 for the dropdown
	// transformOptions - a function to transform the getSelectOptions result
	// class - to use for css column integration
	// style - to use for style - both in thead and tbody
	// required
	// asterisk
	// disabled
	// readOnly
	// disabledToggle
	// displayShowHideButton -

	@Input() properties: any;
	@Input() model: any;

	guid: any;
	//model: any = {};
	selectedItem: any;
	options: any = [];
	items: any = [];
	inputIdx: any = {};
	inputRefresh: any = {};
	reachedEnd: boolean;
	display: any;
	contextMenuOptions: any = {};
	readonly: boolean = false;
	parentScope: any;
	parentForm: any;
	isTransfer: boolean = false;
	masterColumnId: number = null;
	selectAjaxId: number = null;

	// za broadcast
	@Output() onChange: EventEmitter<any> = new EventEmitter();
	@Output() onCreate: EventEmitter<any> = new EventEmitter();
	@Output() onUpdate: EventEmitter<any> = new EventEmitter();
	@Output() onDelete: EventEmitter<any> = new EventEmitter();
	@Output() onItemSelected: EventEmitter<any> = new EventEmitter();
	@Output() onServiceUnsuccessful: EventEmitter<any> = new EventEmitter();
	@Output() onItemEmptied: EventEmitter<any> = new EventEmitter();

	input: any = {};

	// @ViewChild(MatMenuTrigger) contextMenu: MatMenuTrigger;

	stateCtrl: FormControl;
	filteredStates: Observable<any[]>;

	states: any[] = [
		{
			id: 1,
			name: 'Arkansas',
			population: '2.978M',
			// https://commons.wikimedia.org/wiki/File:Flag_of_Arkansas.svg
			flag: 'https://upload.wikimedia.org/wikipedia/commons/9/9d/Flag_of_Arkansas.svg'
		},
		{
			id: 2,
			name: 'California',
			population: '39.14M',
			// https://commons.wikimedia.org/wiki/File:Flag_of_California.svg
			flag: 'https://upload.wikimedia.org/wikipedia/commons/0/01/Flag_of_California.svg'
		},
		{
			id: 3,
			name: 'Florida',
			population: '20.27M',
			// https://commons.wikimedia.org/wiki/File:Flag_of_Florida.svg
			flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Florida.svg'
		},
		{
			id: 4,
			name: 'Texas',
			population: '27.47M',
			// https://commons.wikimedia.org/wiki/File:Flag_of_Texas.svg
			flag: 'https://upload.wikimedia.org/wikipedia/commons/f/f7/Flag_of_Texas.svg'
		}
	];

	contextMenuPosition = { x: "0px", y: "0px" };

	constructor(
		private el: ElementRef,
		private loader: NgModuleFactoryLoader,
		private router: Router,
		public renderer: Renderer2
	) {
		this.guid = "master-detail-" + this.uuidv4();

		this.getDetailValues = this.getDetailValues.bind(this);



		this.getValuesForInput = this.getValuesForInput.bind(this);
	}

	filterStates(name: string) {
		return this.states.filter(state =>
			state.name);
	}

	ngOnInit() {


		this.stateCtrl = new FormControl();
		this.filteredStates = this.stateCtrl.valueChanges.pipe(
			startWith(''),
			map(state => state ? this.filterStates(state) : this.states.slice())
		);

		this.properties.getAllItemsForPrint = function () {
			let list = [];
			for (let i = 0; i < this.items.length; i++) {
				let val = "";
				for (let j = 0; j < this.properties.inputs.length; j++) {
					if (this.items[i][this.properties.inputs[j].displayColumn] != null) {
						val =
							val +
							" " +
							this.items[i][this.properties.inputs[j].displayColumn];
					}
				}
				list.push({ value: val.replace(/null/g, "") });
			}
			return list;
		};

		this.properties.getAllItemsForPrintTherapy = function () {
			let list = [];
			for (let i = 0; i < this.items.length; i++) {
				let val =
					this.items[i].displayName +
					" " +
					this.items[i].comment +
					" " +
					this.items[i].dosage +
					"        ОД " +
					this.items[i].odFormatted;
				if (
					this.items[i].doFormatted != null &&
					this.items[i].doFormatted != ""
				) {
					val = val + " ДО " + this.items[i].doFormatted;
				}
				list.push({ value: val.replace(/null/g, "") });
			}
			return list;
		};

		this.properties.getAllItemsForPrintAsTable = function () {
			if (this.items.length > 0) {
				let table = [];
				this.row = {};
				this.row.children = [];

				for (let j = 0; j < this.properties.inputs.length; j++) {
					let input = this.properties.inputs[j];

					if (!input.notForPrint)
						this.row.children.push({ value: input.label });
				}

				table.push(this.row);
				this.row.children = [];

				for (let i = 0; i < this.items.length; i++) {
					for (let j = 0; j < this.properties.inputs.length; j++)
						if (!this.properties.inputs[j].notForPrint)
							if (this.items[i][this.properties.inputs[j].displayColumn])
								this.row.children.push({
									value: this.items[i][this.properties.inputs[j].displayColumn]
								});
							else this.row.children.push({ value: "" });

					table.push(this.row);
					this.row.children = [];
				}

				return table;
			} else return null;
		};

		this.properties.getAllItemsForPrintAsString = function () {
			let string = "";
			for (let i = 0; i < this.items.length; i++) {
				for (let j = 0; j < this.properties.inputs.length; j++) {
					if (i > 0) {
						string = string + ", ";
					}
					string =
						string + this.items[i][this.properties.inputs[j].displayColumn];
				}
			}
			return string;
		};

		this.properties.getAllItemsForPrintAsHtml = function () {
			let string = "";
			for (let i = 0; i < this.items.length; i++) {
				for (let j = 0; j < this.properties.inputs.length; j++) {
					if (this.items[i][this.properties.inputs[j].displayColumn]) {
						if (i > 0) {
							string = string + "<br/>";
						}
						string =
							string + this.items[i][this.properties.inputs[j].displayColumn];
					}
				}
			}
			string;
			return string;
		};

		this.properties.getAllItemsForPrintTherapyAsHtml = function () {
			let string = "";
			for (let i = 0; i < this.items.length; i++) {
				let val =
					this.items[i].displayName +
					" " +
					this.items[i].comment +
					" " +
					this.items[i].dosage +
					"        ОД " +
					this.items[i].odFormatted;
				if (
					this.items[i].doFormatted != null &&
					this.items[i].doFormatted != ""
				) {
					val = val + " ДО " + this.items[i].doFormatted;
				}
				val = val + "<br/>";
				string += val;
			}
			// string;
			return string;
		};

		this.properties.getAllItemsForPrintAsHtml2 = function () {
			let list = [];
			for (let i = 0; i < this.items.length; i++) {
				let val = "";
				for (let j = 0; j < this.properties.inputs.length; j++) {
					if (this.items[i][this.properties.inputs[j].displayColumn] != null) {
						val =
							val +
							" " +
							this.items[i][this.properties.inputs[j].displayColumn];
					}
				}
				list.push({ value: val });
			}
			let lastString = "";
			for (let i = 0; i < list.length; i++) {
				lastString = lastString + list[i].value + "<br/>";
			}
			return lastString;
		};

		//////////////////////////////

		if (this.properties.tableHeight == undefined) {

			this.properties.tableHeight = "150px";
		}

		if (this.properties.displayShowHideButton == undefined) {
			this.properties.displayShowHideButton = false;
		} else {
			$(".mdTitle").css("padding-top", "5px");
			$("#btnWrapper").css("padding-top", "5px");
		}

		if (this.properties.resizable) {
			setTimeout(function () {
				let width = this.el.find(".box").width();
				let height = this.el.find(".box").height();

				this.loader.load("/Content/themes/base/resizable.css").then(() => {
					let options = this.properties.resizable;

					options = this.extend(options, {
						maxWidth: width,
						minHeight: height,
						minWidth: width
					});
					//$(abp.utils.formatString("#master-detail-{0}", this.guid)).resizable(options);
				});
			});
		}

		// if (!this.properties.disableWatcher)
		//             var modelWatcher = this.$watchCollection('model', function (newValue) {
		//                 if (newValue != null && newValue.id != null && !isTransfer) {
		//                     modelChanged();
		//                     modelWatcher();
		//                 }
		//             });

		if (this.properties.inputs == null) throw "inputs must be provided";

		if (this.properties.service == null) throw "service must be provided";

		if (this.properties.formName) {
			// this.parentForm = this.findFormByName(this);
		} else if (this.properties.jciFormName) {
			this.parentForm = this.findFormByName(this, true);
		}

		this.properties.valid = true;

		this.getOptions();
		// this.watchCombo();

		this.properties.toggleDisabled = this.toggleDisabled;

		this.properties.newItem = this.newItem;

		this.properties.validate = this.validate;

		this.properties.transferFrom = this.transferFrom;

		this.properties.prepareTransfer = this.prepareTransfer;

		this.properties.clear = this.clear;

		this.properties.getValues1 = this.getDetailValues;

		this.properties.getValues = this.getDetailValues(this.masterColumnId);

		this.properties.addItem = function (item) {
			this.items.push(item);
		};

		this.properties.reload = function () {
			if (this.model == null || this.model.id == null) {
				this.clearItems();
			}
			else {
				this.getDetailValues(this.masterColumnId);
			}
		}

		this.properties.reload = this.properties.reload.bind(this);



		this.properties.getItems = function () {
			return this.items;
		};

		this.properties.duplicate = function () {
			for (let i = 0; i < this.items.length; i++) {
				let item = this.items[i];
				item.id = null;
				item.isDuplicate = true;
				if (item.id == null) {
					this.create(item);
				}
			}
		};
	}

	findFormByName(scp, fromJci) {
		if (fromJci)
			if (
				scp.$parent.forms &&
				scp.$parent.forms[this.properties.jciFormName] == undefined
			) {
				this.findFormByName(scp.$parent, true);
			} else {
				this.parentScope = scp.$parent;
				this.registerInParent();
				return scp.$parent.forms[this.properties.jciFormName];
			}
		else if (scp.$parent[this.properties.formName] == undefined) {
			//   this.findFormByName(scp.$parent);
		} else {
			this.parentScope = scp.$parent;
			this.registerInParent();
			return scp.$parent[this.properties.formName];
		}
	}

	registerInParent() {
		if (this.parentScope.masterDetailList) {
			this.parentScope.masterDetailList.push(this.properties);
		}
	}

	uuidv4() {
		return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
			let r = (Math.random() * 16) | 0,
				v = c == "x" ? r : (r & 0x3) | 0x8;
			return v.toString(16);
		});
	}

	itemPropertyChange(col, item) {
		if (col.onChange) {
			col.onChange.emit(item); //broadcast
		}
	}

	navisionInputChange(item, input) {
		if (input && input.onChange) input.onChange.emit({ item: item }); //broadcast
	}

	getOptions() {
		let reqList = [];
		if (this.properties.inputs != null) {
			for (let i = 0; i < this.properties.inputs.length; i++) {
				let input = this.properties.inputs[i];

				if (input.type == "select" && !input.options) {
					let t = i;
					let req = this.getValuesForInput(t);
					reqList.push(req);

					this.inputIdx[input.column] = i;

					input.refresh = function () {
						let self = this;
						let idx = this.inputIdx[self.column];
						this.inputRefresh[self.column] = true;

						return this.getValuesForInput(idx);
					};
				} else if (input.type == "navision" && !input.functions) {
					input.functions = {};
				}
			}
		}
	}

	getValuesForInput(idx: number) {
		let input = this.properties.inputs[idx];
		if (input.dropDownName != null) {
			this.addContextMenuOptions(input);
			// return this.mstrKeyValuesService
			// 	.getListValues(
			// 		input.dropDownName,
			// 		undefined,
			// 		undefined,
			// 		undefined,
			// 		undefined,
			// 		0,
			// 		10
			// 	)
			// 	.subscribe(result => {
			// 		debugger;
			// 		this.handleOptionsResult(result, input);
			// 	});
		} else if (input.dropDownStationaryName != null) {
			this.addContextMenuOptions(input);
			// return this.mstrKeyValuesService
			// 	.getListValues(
			// 		input.dropDownStationaryName,
			// 		undefined,
			// 		undefined,
			// 		undefined,
			// 		undefined,
			// 		0,
			// 		10
			// 	)
			// 	.subscribe(result => {
			// 		this.handleOptionsResult(result, input);
			// 	});
		} else if (input.getSelectOptions != null) {
			debugger;
			if (input.link != null) {
				this.addContextMenuOptionsNotDropDown(input);
			}

			// popolnuvanje na dropdown ako ima getSELECTEDoptions

			// let req = input.getSelectOptions;

			// return req().subscribe(result => {
			// 	debugger;
			// 	this.handleOptionsResult(result, input);
			// });






			return input.getSelectOptions.getAllForDropdown().subscribe((result) => {
				this.handleOptionsResult(result, input);
			});



		} else if (input.getStaticSelectOptions != null) {
			this.handleOptionsResult(input.getStaticSelectOptions, input);
		}
	}

	addContextMenuOptions(input) {
		// if (!this.contextMenuOptions[input.column]) {
		//   debugger;
		//   this.contextMenuOptions[input.column] = [
		//     {
		//       text: 'Вредности на паѓачка листа',
		//       click: function () {
		//         this.dropDownService.getByName(input.dropDownName || input.dropDownStationaryName).subscribe((result) => {
		//           if (result.name.toLowerCase().startsWith('stationary')) {
		//             this.router.navigate(['app/lookup/citizenship'], {
		//               queryParams: { stationaryDropDownId: result.id }
		//             });
		//             //$state.go('administration.stationaryDropDownValuesId', { stationaryDropDownId: result.id });
		//             //window.open("/#/administration/stationaryDropDownValues/" + result.id);
		//           }
		//           else {
		//             this.router.navigate(['app/lookup/citizenship'], {
		//               queryParams: { stationaryDropDownId: result.id }
		//             });
		//             //$state.go('administration.dropDownValuesId', { dropDownId: result.id });
		//             // window.open("/#/administration/dropDownValues/" + result.id);
		//           }
		//         })
		//       }
		//     },
		//     {
		//       text: 'Освежи паѓачка листа',
		//       click: function () {
		//         let inputIdx = this.properties.inputs.findIndex(t => t.column == input.column);
		//         if (inputIdx != -1) {
		//           this.getValuesForInput(inputIdx);
		//         }
		//       }
		//     }
		//   ]
		// }
	}

	addNewValuesForDropDown(input) {
		// this.mstrKeyService
		// 	.getByName(input.dropDownName || input.dropDownStationaryName)
		// 	.subscribe(result => {
		// 		// window.open("app/lookup/dropDownValues/" + result.id);
		// 		window.open("/registers/key-values/" + result.id);

		// 		// if (result.name.toLowerCase().startsWith('stationary')) {

		// 		//   // this.router.navigate(['app/lookup/key-values'], {
		// 		//   //   queryParams: { stationaryDropDownId: result.id }
		// 		//   // });

		// 		//   //$state.go('administration.stationaryDropDownValuesId', { stationaryDropDownId: result.id });
		// 		// window.open("app/lookup/key-values/" + result.id);
		// 		// }
		// 		// else {

		// 		//   window.open("app/lookup/key-values/" + result.id);
		// 		//   // this.router.navigate(['app/lookup/key-values'], {
		// 		//   //   queryParams: { stationaryDropDownId: result.id }
		// 		//   // });

		// 		//   //$state.go('administration.dropDownValuesId', { dropDownId: result.id });
		// 		//   // window.open("/#/administration/dropDownValues/" + result.id);
		// 		// }
		// 	});
	}

	refreshValuesForDropDown(input) {
		let inputIdx = this.properties.inputs.findIndex(
			t => t.column == input.column
		);

		if (inputIdx != -1) {
			this.getValuesForInput(inputIdx);
		}
	}

	addContextMenuOptionsNotDropDown(input) {
		if (!this.contextMenuOptions[input.column]) {
			this.contextMenuOptions[input.column] = [
				{
					text: "Вредности на паѓачка листа",
					click: function () {
						window.open(input.link);
					}
				},
				{
					text: "Освежи паѓачка листа",
					click: function () {
						let inputIdx = this.properties.inputs.findIndex(
							t => t.column == input.column
						);
						if (inputIdx != -1) {
							this.getValuesForInput(inputIdx);
						}
					}
				}
			];
		}
	}

	onContextMenu(event: MouseEvent, input: any) {
		event.preventDefault();
		this.contextMenuPosition.x = event.clientX + "px";
		this.contextMenuPosition.y = event.clientY + "px";
		// this.contextMenu.menuData = { item: input };
		// this.contextMenu.openMenu();
	}

	watchCombo() {
		// let x = this;
		// setInterval(function () {
		//   for (let input of x.properties.inputs) {
		//     if (input.dropDownName || input.dropDownStationaryName || input.link) {
		//       let span = $("#" + x.guid + "_" + input.column).siblings('span.select2');
		//       if (span && !span.attr('context-menu')) {
		//         span.attr('context-menu', "contextMenuOptions['" + input.column + "']");
		//         //$compile(span)($scope);
		//       }
		//     }
		//   }
		// }, 1000);
	}

	handleOptionsResult(result, input) {
		let data = result;
		if (input.transformOptions != null) {
			data = input.transformOptions(result);
		}

		input.options = data;
		if (!this.inputRefresh[input.column]) {
			//console.log(input.label, input);

			if (input.options && input.options.length > 0 && input.required == true) {
			} else {
				input.value = "";
			}
		} else this.inputRefresh[input.column] = false;

		this.inputChange(input);
	}

	// load na podatocite vo master tableata

	getDetailValues(masterColumnId) {
		debugger;
		let input = {
			maxResultCount: 999
		};

		if (this.properties.onBeforeGet != null)
			input = this.properties.onBeforeGet(input);

		input[this.properties.masterColumn] = masterColumnId || this.model.id;

		let elem = this.el.nativeElement.getElementsByClassName(
			"box box-solid box-light"
		);

		this.renderer.setAttribute(elem[0], "id", this.guid);
		debugger;

		let req = this.properties.service.search(input).subscribe(

			res => {
				abp.ui.setBusy($("#" + this.guid));
				this.items = res.items;
				this.newItem();
				this.onChange.emit({ items: this.items }); //broadcast
				abp.ui.clearBusy($("#" + this.guid));
			},
			error => {
				abp.ui.clearBusy($("#" + this.guid));
			}
		);
		// 

		return req;
	}

	setBusy(req) {
		abp.ui.setBusy($("#master-detail-" + this.guid), req);
	}

	// $scope.properties.sync = modelChanged;

	modelChanged() {
		if (this.model != null && this.model.id != null) {
			if (this.items.length == 0) {
				this.getDetailValues(this.masterColumnId);
			} else {
				this.createItems();
			}
		}
	}

	createItems() {
		for (var i = 0; i < this.items.length; i++) {
			var item = this.items[i];
			if (item.id == null) {
				this.create(item);
			}
		}
	}

	prepareTransfer() {
		this.isTransfer = true;
	}

	save() {
		debugger;

		let obj = {};
		let y = 0;
		let elements = this.el.nativeElement.getElementsByClassName("select2-ajax");

		let hasValue = this.properties.inputs.filter(item => item.value != null).length > 0;

		// if (!hasValue)
		//   return;

		let isValid = true;
		this.properties.inputs.forEach(item => {
			if (item.required == true && item.value == null) {
				isValid = false;
			}
		});

		if (!isValid) {
			abp.message.warn("Внесете податок во задолжителното поле", "Известување");
			return;
		}

		//let defer = false;
		//this.reachedEnd = false;

		for (let i = 0; i < this.properties.inputs.length; i++) {
			let input = this.properties.inputs[i];

			obj[input.column] = input.value;

			if (input.type == "select" && input.value) {
				this.display = "";

				if (input.selectAjaxOptions != null) {

					this.display = input.functions.getSelectedText(elements[y].id);

					obj[input.column] = input.functions.getSelectedValue(elements[y].id);
					y++;
				} else {
					let selValue = _.find(input.options, function (el) {
						return el.id == input.value;
					});
					if (selValue) {
						this.display = selValue.name;
					}
				}

				obj[input.displayColumn] = this.display;
			}
			else if (input.type == "date" || input.type == "time") {
				obj[input.displayColumn] = moment(input.value).format("DD.MM.YYYY");
			}
			else {
				if (input.type == "select" && input.selectAjaxOptions) {
					this.display = input.functions.getSelectedText(elements[y].id);

					obj[input.column] = input.functions.getSelectedValue(elements[y].id);
					y++;
					obj[input.displayColumn] = this.display;
				} else {
					if (obj[input.column] == null) {
						obj[input.column] = "";
					} else {
						obj[input.column] = input.value;
					}
				}
			}
		}

		// if (!defer) 
		this.save1(obj);

		//this.reachedEnd = true;
	}

	save1(obj) {
		if (this.selectedItem != null) {
			this.selectedItem = Object.assign(this.selectedItem, obj);

			if (this.selectedItem.id != null) {
				if (this.properties.onBeforeUpdate != null)
					//&& $scope.selectedItem.breakCode != true)
					this.selectedItem = this.properties.onBeforeUpdate(this.selectedItem);

				if (this.selectedItem.breakCode != true) {

					abp.ui.setBusy(
						$("#" + this.guid),
						this.properties.service.update(this.selectedItem).subscribe(
							(res: any) => {
								this.selectedItem = Object.assign(this.selectedItem, res.data);
								this.properties.selectedItem = this.selectedItem;
								this.validate();
								this.onUpdate.emit({ item: this.selectedItem }); //broadcast
								this.onChange.emit({ items: this.items }); // broadcast
								abp.ui.clearBusy($("#" + this.guid));
							},
							function () {
								if (this.selectedItem.refreshOnUnsuccessful == true) {

									this.onServiceUnsuccessful();
									this.properties.reload();
									abp.ui.clearBusy($("#" + this.guid));
								}
								this.selectedItem = obj;
							}
						)
					);

					// this.properties.service.update(this.selectedItem)

					//   .subscribe((res) => {
					//     //$rootScope.$broadcast('mdSaveCompleted');

					//     this.selectedItem = Object.assign(this.selectedItem, res.data);
					//     this.properties.selectedItem = this.selectedItem;

					//     this.validate();

					//     this.onUpdate.emit({ item: this.selectedItem }); //broadcast
					//     this.onChange.emit({ items: this.items }); // broadcast

					//   }, function () {
					//     if (this.selectedItem.refreshOnUnsuccessful == true) {
					//       this.onServiceUnsuccessful();

					//       this.properties.reload();
					//     }

					//     this.selectedItem = obj;
					//   });
				}
			}
		} else {
			obj = Object.assign(obj, this.properties.selectedItem);
			this.items.push(obj);
			this.newItem();
			this.validate();
			this.create(obj);
		}
	}

	transferFrom(examination, newExamination) {
		//var deferred = $q.defer();

		this.getDetailValues(examination.id).subscribe(result => {
			this.model = newExamination;
			setTimeout(() => {
				this.items = result.items;
				//console.log('md-items', $scope.items);
				this.items.forEach(item => {
					item.id = null;
				});

				this.createItems();
				// deferred.resolve(result);
			});
		});

		// return deferred.promise;
	}

	append(list) {
		for (var item of list) {
			this.items.push(item);
			this.create(item);
		}
	}

	create(item) {
		debugger;
		this.onCreate.emit({ item: item }); //broadcast
		this.onChange.emit({ items: this.items }); //broadcast
		if (this.model != null && this.model.id != null) {
			item[this.properties.masterColumn] = this.model.id;

			debugger;

			if (this.properties.onBeforeCreate != null)
				item = this.properties.onBeforeCreate(item);

			if (item) {
				abp.ui.setBusy(
					$("#" + this.guid),
					this.properties.service.create(item).subscribe(
						res => {
							if (this.properties.onAfterCreate != null)
								item = this.properties.onAfterCreate(res);
							item.id = res.id;
							this.validate();
							abp.ui.clearBusy($("#" + this.guid));
						},
						error => {
							abp.ui.clearBusy($("#" + this.guid));
							this.items.splice(-1, 1);
						}
					)
				);
			} else {
				this.items.splice(-1, 1);
			}
		}
	}

	toggleDisabled(action) {
		// let inputsWithDisabledToggle = _.filter(this.properties.inputs, function (o) { return o.disabledToggle; });
		// if (inputsWithDisabledToggle && inputsWithDisabledToggle.length > 0) {
		//   inputsWithDisabledToggle.forEach(z => {
		//     if (action != 'clearDisabledWithoutSelectedItem') {
		//       let itm = this.selectedItem ? this.selectedItem[z.column] : this.properties.selectedItem ? this.properties.selectedItem[z.column] : null;
		//       let input = this.properties.inputs.find(o => o.column == false); //z.disabledToggle
		//       if (action == 'clearDisabled') {
		//         if (itm == undefined || itm == '' || itm == null)
		//           input.disabled = null;
		//       }
		//       else if (action == 'makeDisabled') {
		//         if (itm && itm !== '')
		//           input.disabled = true;
		//       }
		//       else if (action == 'clearDisabledWithoutSelectedItem')
		//         input.disabled = null;
		//       else
		//         throw 'no action provided';
		//     }
		//   });
		// }
	}

	inputChange(input) {
		if (input.onChange && (input.options || input.selectAjaxOptions != null)) {
			if (
				input.selectAjaxOptions == null ||
				input.selectAjaxOptions == undefined
			) {
				let obj = input.options.find(item => item.id == input.value);
				if (obj) input.onChange(obj, this.properties);
				else input.onChange(input.value);
			} else input.onChange(input);
		}
	}

	// getEmmittedAjaxId(event: any) {
	//   this.selectAjaxId = parseInt(event);
	//   console.log(this.selectAjaxId);
	// }

	selectItem(item) {
		debugger;
		let y = 0;
		let elements = this.el.nativeElement.getElementsByClassName("select2-ajax");

		this.selectedItem = item;
		this.toggleDisabled("clearDisabled");

		for (let i = 0; i < this.properties.inputs.length; i++) {
			let inp = this.properties.inputs[i];
			inp.value = this.selectedItem[inp.column];

			if (inp.type == "date" || inp.type == "time") {
				inp.value = new Date(this.selectedItem[inp.column]);
				// inp.value = this.selectedItem[inp.displayColumn];
			}

			if (inp.selectAjaxOptions != null) {
				inp.functions.setOptions([
					{
						value: inp.value,
						text: this.selectedItem[inp.displayColumn],
						elementId: elements[y].id
					}
				]);
				y++;
			}
			this.properties.selectedItem = this.selectedItem;
			this.inputChange(inp);
		}

		this.toggleDisabled("makeDisabled");
		this.onItemSelected.emit(this.selectedItem); // broadcast
	}

	newItem() {
		this.toggleDisabled("clearDisabledWithoutSelectedItem");

		let y = 0;
		let elements = this.el.nativeElement.getElementsByClassName("select2-ajax");

		this.selectedItem = null;
		this.properties.selectedItem = null;
		for (let i = 0; i < this.properties.inputs.length; i++) {
			let input = this.properties.inputs[i];
			if (input.type == "select") {
				if (
					input.selectAjaxOptions != null &&
					input.functions &&
					input.functions.setOptions
				) {
					input.functions.setOptions([{ elementId: elements[y].id }]);
					y++;
				} else if (
					input.options &&
					input.options.length > 0 &&
					input.required == true
				) {
					//input.value = input.options[0].id;
					input.value = null;
				} else input.value = null;
			} else if (input.type == "time" || input.type == "date") {
				if (input.noTimeInitialization == true) {
					input.value = null;
				} else {
					if (input.type == "time") {
						input.value = moment(new Date()).format("HH:mm");
					} else {
						input.value = moment(new Date()).format("YYYY-MM-DD");
					}
				}
			} else {
				input.value = null;
			}
		}

		if (this.properties.onNew) this.properties.onNew();

		this.onItemEmptied.emit(); //broadcast

		// $scope.$emit('MDnewItemClicked');
	}

	alert() {
		alert("change");
	}

	deleteItem(record) {
		if (this.selectedItem == null && record == null) return;

		let index;
		if (record == null)
			for (let i = 0; i < this.items.length; i++) {
				if (
					this.items[i] == this.selectedItem ||
					(this.items[i].id != null && this.items[i].id == this.selectedItem.id)
				) {
					index = i;
					break;
				}
			}

		if (this.model != null && this.model.id != null) {
			let item = record || this.items[index];

			if (item.id) {
				abp.ui.setBusy(
					$("#" + this.guid),
					this.properties.service.delete(item.id).subscribe(
						() => {
							// $rootScope.$broadcast('mdSaveCompleted');
							this.deleteAndValidate(index);
							abp.ui.clearBusy($("#" + this.guid));
						},
						error => {
							abp.ui.clearBusy($("#" + this.guid));
						}
					)
				);
			} else this.deleteAndValidate(index);
		} else this.deleteAndValidate(index);

		this.selectedItem = null;
		this.properties.selectedItem = null;

		this.newItem();

		this.onItemEmptied.emit(); //broadcast

		// $rootScope.$broadcast('mdSaveCompleted');
	}

	deleteAndValidate(index) {
		this.deleteItem1(index);
		this.validate();
	}

	deleteItem1(index: number) {
		this.items.splice(index, 1);
		this.onDelete.emit({ item: this.items[index] });
		this.onChange.emit({ items: this.items });
	}

	clear() {
		this.items.forEach(item => this.deleteItem(item));
	}

	clearItems() {
		this.items = [];
	}

	validate() {
		this.properties.valid = this.items.length != 0 || !this.properties.required;
		return this.properties.valid;
	}

	isReadonly() {
		return false;
	}

	changeText() {
		let elem = this.el.nativeElement.getElementsByClassName("btnToggle");
		let elem1 = this.el.nativeElement.getElementsByClassName("btnWrapper");

		if (elem[0].innerHTML == "Затвори") {
			elem[0].innerHTML = "Отвори";
			elem1[0].style = "display: none";
		} else {
			elem[0].innerHTML = "Затвори";
			elem1[0].style = "display: block";
		}
	}
}
