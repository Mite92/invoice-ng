import { Directive, EventEmitter, Output, HostListener, OnInit } from '@angular/core';

@Directive({
  selector: '[appTest]'
})

export class TestDirective implements OnInit{

  @Output() itch:EventEmitter<any> = new EventEmitter();
  
 @HostListener('onClick') onClick(){
///We are emitting itchies!!
    this.itch.emit('itch itch itch');
  }
 

  constructor() { }

  ngOnInit(){
    this.itch.emit('itch itch itch');
  }

}
