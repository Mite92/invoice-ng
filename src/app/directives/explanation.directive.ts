import { Directive, Input, ElementRef, OnInit, HostListener, Renderer2 } from '@angular/core';


@Directive({
	selector: '[explanation]'
})
export class ExplanationDirective implements OnInit {


	@Input('explanation') explanation: string;
	@Input('explanation-title') explanationTitle: string;
	@Input('explanation-image') explanationImage: string;
	@Input('explanation-icon') explanationIcon: string;
	@Input('explanation-placement') explanationPlacement: string;

	constructor(public el: ElementRef,
		public renderer: Renderer2) {
	}

	@HostListener('mouseenter') onMouseEnter() {
		// alert(this.el.nativeElement.title);
		// this.highlight(this.highlightColor || 'red');
	}

	ngOnInit() {

		let title = "";
		let icon = "";
		let content = this.el.nativeElement.getAttribute('explanation');


		let template = '<p class="explanation-title">{{title}}</p><div class="row"><div class="col-md-1"><span class="{{icon}} explanation-icon"></span></div><div class="col-md-10 explanation-content">{{content}}</div></div>';

		let imgTemplate = '<p class="explanation-title">{{title}}</p><div class="row"><div class="col-md-1"><img src="{{imgUrl}}" class="explanation-image" height="24px;" width="24px;"/></div><div class="col-md-10 explanation-content">{{content}}</div></div>';


		if (this.explanationTitle) {
			title = this.explanationTitle;
		}

		if (this.explanationImage) {
			template = imgTemplate;
			template = template.replace("{{imgUrl}}", this.explanationImage);
		}

		if (this.explanationIcon) {
			icon = this.explanationIcon;
		}

		template = template.replace("{{title}}", title);
		template = template.replace("{{icon}}", icon);
		template = template.replace("{{content}}", content);

		this.renderer.removeAttribute(this.el.nativeElement, 'explanation');

		this.renderer.setAttribute(this.el.nativeElement, 'data-toggle', 'tooltip');
		this.renderer.setAttribute(this.el.nativeElement, 'data-html', 'true');
		this.renderer.setAttribute(this.el.nativeElement, 'title', template);

		if (this.explanationPlacement) {
			this.renderer.setAttribute(this.el.nativeElement, 'data-placement', this.explanationPlacement);
		}

	}
}