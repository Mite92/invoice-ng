import { Component, OnInit, Input, EventEmitter, Output, ComponentFactoryResolver, ViewChild, TemplateRef, Injector } from '@angular/core';
import { Location, LocationStrategy, PathLocationStrategy } from '@angular/common';

import { Subscription } from 'rxjs';
import { Route, Router, ActivatedRoute } from '@angular/router';
import { PagedResultDto, PagedRequestDto } from '@shared/paged-listing-component-base';

import * as  moment from 'moment';
import { finalize } from 'rxjs/operators';
import { MatDialog, MatDialogRef, DateAdapter } from '@angular/material';
import { isNumber } from 'util';
import { AppComponentBase } from '@shared/app-component-base';

@Component({
	selector: 'custom-table',
	templateUrl: './custom-table.component.html',
	styleUrls: ['./custom-table.component.scss']
})

export class CustomTableComponent extends AppComponentBase implements OnInit {
	/* tslint:disable */
	@Input() tableProperties: any;

	column: boolean = true;

	dis: string = "block";

	columns: any;
	data: Array<any>;
	pageNumbers = [10, 20, 50, 100];
	totalCount: number;
	req: Subscription;
	selectedItem = {};
	selectedItemIndex: number;
	selectedItemSequenceNumber: number = 1;
	currentPage = 1;
	currentSort = null;
	isResizing = false;
	itemsPerPage = this.pageNumbers[0];
	page: number = 1;
	isInitial: boolean = true;
	customSearchProps: any = [];
	pageOfItems: Array<any>;
	items = [];
	input: any = {};
	search: string;
	tableConfigJson: any = {};
	storedValues: any;
	showInactive: boolean = false;
	getDataOnStart = true;
	public totalPages = 1;
	public totalItems: number;
	public pageNumber = 1;
	public isTableLoading = false;
	params: any = {};
	resizeMode = "OverflowResizer";



	@Output() model: EventEmitter<any> = new EventEmitter();

	constructor(
		private router: Router,
		private route: ActivatedRoute,
		private _dialog: MatDialog,
		private componentFactoryResolver: ComponentFactoryResolver,
		public dialog: MatDialog,
		injector: Injector,
		private dateAdapter: DateAdapter<Date>) {

		//zemanje na resolve data od url
		super(injector);

		dateAdapter.setLocale('en-GB'); // DD/MM/YYYY


		this.getData = this.getData.bind(this);
	}

	al() {
		this.dis = 'none';
		alert(this.dis);
	}

	ngOnInit() {

		let params = this.route.queryParams.subscribe(params => this.params = params);


		if (this.params) {

			if (this.params.maxResultCount) {

				this.itemsPerPage = + this.params.maxResultCount;

				//this.route.queryParams.subscribe( params => params['maxResultCount'].remove());

			}
			if (this.params.skipCount) {
				this.currentPage = (+ this.params.skipCount / this.itemsPerPage) + 1;
			}
			if (this.params.sorting) {
				this.currentSort = this.params.sorting;
			}
			if (this.params.search) {
				this.search = this.params.search;
			}
			if (this.params.recordIdx) {
				this.selectedItemIndex = +this.params.recordIdx;
			}

			for (let key in this.params) {
				if (!isNaN(this.params[key]) && isNumber(+ this.params[key])) {

					this.customSearchProps[key] = parseInt(this.params[key]);
				}
				else {
					if (this.params[key] == 'NaN') {
						this.params[key] = "";
					}
					this.customSearchProps[key] = this.params[key];
				}
			}
		}




		let x = this;

		this.columns = x.tableProperties.columns;



		//this.getData();

		this.tableProperties.allowOrder = this.tableProperties.allowOrder != null ? this.tableProperties.allowOrder : true;
		this.tableProperties.allowSearch = this.tableProperties.allowSearch != null ? this.tableProperties.allowSearch : true;
		this.tableProperties.customSearch = this.tableProperties.customSearch != null ? this.tableProperties.customSearch : false;

		if (this.tableProperties.customSearch) {
			this.tableProperties.allowSearch = false;
		}


		this.tableProperties.emptyData = function () {
			x.emptyData();
		}

		this.tableProperties.refresh = function () {
			x.getData();
		}

		this.tableProperties.updateItem = function (item) {
			this.data.forEach((el, idx) => {
				if (el.id == item.id) {
					this.data[idx] = Object.assign(this.data[idx], item);
				}
			});
		}

		this.tableProperties.clearSelection = function () {
			x.selectedItem = null;
		}

		x.storedValues = localStorage.getItem(this.getStorageKey());

		if (x.storedValues) {
			x.tableConfigJson = JSON.parse(x.storedValues);
		}


		this.tableProperties.getDataWithChangedColumns = function () {
			this.columns = this.tableProperties.columns;
			this.getData();
		}





		if (x.tableProperties.defaultMaxResultCount) {
			this.itemsPerPage = x.pageNumbers[this.tableProperties.defaultMaxResultCount];
		}
		else if (this.tableConfigJson.itemsPerPage) {
			x.itemsPerPage = x.tableConfigJson.itemsPerPage;
		}
		else {
			x.itemsPerPage = x.pageNumbers[0];
		}

		// if (x.getDataOnStart)
		x.getData();


	}



	showData(result: any) {
		if (result.totalCount) {
			this.totalCount = result.totalCount;
		} else {
			this.totalCount = result.length;
		}

		if (result.items) {

			this.data = result.items;
		} else {
			this.data = result;
		}

		if (this.data != null && this.data.length > 0) {

			if (this.isInitial && this.selectedItemIndex && this.data.length > this.selectedItemIndex)
				this.selectItem(this.data[this.selectedItemIndex], this.selectedItemIndex);
			else
				this.selectItem(this.data[0], 0);

		}

		this.isInitial = false;
	}

	selectItem(item: any, itemIndex: number) {

		this.selectedItem = item;
		this.selectedItemIndex = itemIndex;

		if (this.tableProperties.saveState) {
			this.router.navigate([], {
				queryParams: {
					recordIdx: this.selectedItemIndex
				},
				queryParamsHandling: 'merge',
			});
		}

		this.selectedItemSequenceNumber = (this.currentPage - 1) * this.itemsPerPage + itemIndex + 1;

		if (this.tableProperties.onItemSelected) {
			this.model.emit(item);
			this.tableProperties.onItemSelected(item);
		}
	}

	getEmmittedResult(event) {

		this.input[event.propName] = event.id;
		this.getData();

	}

	resolve(value: any, item: any) {
		return item[value]
	}

	reloadDisabled = function () {
		if (this.tableProperties.reloadDisabled)
			return true;
		else
			return false;
	}

	sortOrder(optionasc, optiondesc, disableOrder, $event) {


		if (!(disableOrder == null || disableOrder == false) || this.isResizing) {
			return;
		}

		optionasc = optionasc.charAt(0).toUpperCase() + optionasc.slice(1);
		optiondesc = optiondesc.charAt(0).toUpperCase() + optiondesc.slice(1);


		if (this.currentSort != optionasc) {
			this.currentSort = optionasc;
		}
		else {
			this.currentSort = optiondesc;
		}

		if (this.data.length > 0) {
			this.getData();
		}
	}

	getStorageKey() {
		return `${this.route.routeConfig.component.name}.tableConfig`;
	}

	doubleClickFn(item: any) {
		if (this.tableProperties.onDoubleClick) {

			this.tableProperties.onDoubleClick(item);
		}
	}

	activeFilterChanged(val) {
		this.showInactive = val;
		this.getData();
	}

	firstUC(value) {
		//  return value.charAt(0).toUpperCase() + value.slice(1);
	}


	getData() {

		let x = this;


		let input = {
			SkipCount: ((this.currentPage - 1) * this.itemsPerPage),
			MaxResultCount: this.itemsPerPage,
			Sorting: this.currentSort,
			Search: this.search,
			ShowInactive: null
		}

		// this.input.search = this.search;
		// this.input.sorting = this.currentSort;
		// this.input.maxResultCount = this.itemsPerPage;

		// this.input.skipCount = ((this.currentPage - 1) * this.itemsPerPage);

		localStorage.setItem(this.getStorageKey(), JSON.stringify({ itemsPerPage: this.itemsPerPage }));

		let dates = true;

		for (var propertyName in this.customSearchProps) {
			for (var i = 0; i < this.columns.length; i++) {


				if (this.columns[i].value == propertyName || (this.columns[i].customSearch !== false && this.columns[i].customSearch.field == propertyName)) {

					if (this.columns[i].customSearch.dataType == 'date') {
						if (this.customSearchProps[propertyName] != undefined) {

							if (moment(this.customSearchProps[propertyName], "dd.MM.yyyy").format("DD.MM.YYYY").indexOf('d') > -1) {
								dates = false;
							}
							if (moment(this.customSearchProps[propertyName], "dd.MM.yyyy").format("DD.MM.YYYY").indexOf('m') > -1) {
								dates = false;
							}
							if (moment(this.customSearchProps[propertyName], "dd.MM.yyyy").format("DD.MM.YYYY").indexOf('y') > -1) {
								dates = false;
							}
						}
					}

					if (this.columns[i].isBool) {
						if (this.columns[i].customSearch.dataType == 'boolean') {

							var val = this.customSearchProps[propertyName];
							if (val) {
								var isTrueSet = (val.toLowerCase() == 'true');
								input[propertyName] = isTrueSet;
							}
						}
						else
							input[propertyName + 'String'] = this.customSearchProps[propertyName].toLowerCase();
						//console.log('input2[' + propertyName + ']', input[propertyName]);
					}
					else {
						if (this.customSearchProps[propertyName] != undefined) {
							input[propertyName] = this.customSearchProps[propertyName];

						}
					}

				}
			}
			console.log(input);

		}

		if (this.tableProperties.hasActiveFilter == true) {
			// this.input.showInactive = this.showInactive;
			input.ShowInactive = this.showInactive;
		}

		if (dates) {

			if (this.tableProperties.saveState) {
				debugger;
				this.router.navigate([], {
					// queryParams: this.input,
					queryParams: input,
					//	queryParamsHandling: 'merge'
				});
			}
			let req = x.tableProperties.getData(input);

			abp.ui.setBusy(null,
				req.subscribe((result) => {
					this.showData(result);
					this.showPaging(result, this.pageNumber);
					abp.ui.clearBusy();
				}, (error) => { abp.ui.clearBusy(); }));


		}
	}

	emptyData() {
		this.data = [];
		this.totalCount = 0;
		this.selectedItem = null;
	}

	public showPaging(result: PagedResultDto, pageNumber: number): void {
		this.totalPages = ((result.totalCount - (result.totalCount % this.itemsPerPage)) / this.itemsPerPage) + 1;

		this.totalItems = result.totalCount;
		this.pageNumber = pageNumber;
	}

	public getDataPage(page: number): void {
		const req = new PagedRequestDto();
		req.maxResultCount = this.itemsPerPage;
		req.skipCount = (page - 1) * this.itemsPerPage;
		this.pageNumber = page;
	}

	checkBoxChanged = function () {
		// $scope.$parent.checkBoxChanged();
	}

	customTableBtnClicked(col) {
		//$scope.$parent.customTableBtnClicked(col);
	}

	edit(item: any): void {
		// this.openDialog(this.tableProperties.components);

		this.showCreateOrEditCitizenshipDialog(item.id);
	}


	toggleColumnVisibility(column, event) {

		event.stopPropagation();
		event.stopImmediatePropagation();

	}

	showCreateOrEditCitizenshipDialog(id?: number): void {
		const componentFactory = this.componentFactoryResolver.resolveComponentFactory(this.tableProperties.components);
		//const componentRef = componentFactory.create(this.injector);
		//const componentFactory = this.componentFactoryResolver.resolveComponentFactory();

		let createOrEditCitizenshipDialog;
		if (id === undefined || id <= 0) {
			createOrEditCitizenshipDialog = this._dialog.open(this.tableProperties.components);
		} else {
			createOrEditCitizenshipDialog = this._dialog.open(componentFactory.componentType, {
				data: id
			});
		}

		createOrEditCitizenshipDialog.afterClosed().subscribe(result => {
			if (result) {
				// this.refresh();
			}
		});
	}

	// delete(item: any): void {

	//   abp.message.confirm(
	//     this.l('Are you sure you want to delete this citizenship?', item.displayName),
	//     (result: boolean) => {
	//       if (result) {
	//         this._employeeService
	//           .delete(item.id)
	//           .pipe(
	//             finalize(() => {
	//               abp.notify.success(this.l('SuccessfullyDeleted'));
	//               //this.refresh();
	//             })
	//           )
	//           .subscribe(() => { });
	//       }
	//     }
	//   );
	// }


	L(key: string) {
		return this.l(key);
	}

}



//Usage
//controller:
//$scope.tableProperties = {
//    columns: [
//        { heading: 'Опис', value: 'description' },
//        { heading: 'Реден број', value: 'sequenceNumber' }
//    ],
//    getData: recommendationService.getAll,
//    onItemSelected: onTableItemSelected
//};
//view:
//<custom-table [tableProperties]="properties"></custom-table>

// properties
// customColumns - dali da se prikazuva kopceto za koloni
// visible - dali da se prikaze kolonata
// width  - sirina na kolonata
// customSearch - dali da se prikaze pole za prebaruvanje

