import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CustomTableComponent } from './custom-table.component';
import { MaterialModule } from '@fury/shared/material-components.module';
import { FormsModule, ReactiveFormsModule, FormGroup } from '@angular/forms';
import { PaginationModule } from 'ngx-bootstrap';
import { SharedModule } from '@shared/shared.module';
import { RouterModule } from '@angular/router';
import { LoadingIndicatorModule } from '@fury/shared/loading-indicator/loading-indicator.module';
import { FurySharedModule } from '@fury/fury-shared.module';
import { ToolbarModule } from '@app/layout/toolbar/toolbar.module';
import { QuickpanelModule } from '@app/layout/quickpanel/quickpanel.module';
import { SidenavModule } from '@app/layout/sidenav/sidenav.module';
import { FooterModule } from '@app/layout/footer/footer.module';
import { BackdropModule } from '@fury/shared/backdrop/backdrop.module';
import { ConfigPanelModule } from '@app/layout/config-panel/config-panel.module';
import { NavigationModule } from '@app/layout/navigation/navigation.module';
import { HighlightModule } from '@fury/shared/highlightjs/highlight.module';
import { FuryCardModule } from '@fury/shared/card/card.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { ScrollbarModule } from '@fury/shared/scrollbar/scrollbar.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatDialogModule, MatButtonModule, MatIconModule, MatRadioModule, MatSelectModule, MatTabsModule, MatCheckboxModule } from '@angular/material';
import { ListModule } from '@fury/shared/list/list.module';
import { AbpPaginationControlsComponent } from '@shared/pagination/abp-pagination-controls.component';
import { NgxPaginationModule } from 'ngx-pagination';



@NgModule({
	declarations: [
		CustomTableComponent,
		AbpPaginationControlsComponent,

	],
	imports: [
		CommonModule,
		FormsModule,
		ReactiveFormsModule,
		MaterialModule,
		RouterModule,
		HighlightModule,
		FuryCardModule,
		BreadcrumbsModule,
		ScrollbarModule,
		FlexLayoutModule,
		MatDialogModule,
		MatButtonModule,
		MatIconModule,
		MatRadioModule,
		MatSelectModule,
		MatTabsModule,
		MatCheckboxModule,
		ListModule,
		NgxPaginationModule
	],
	exports: [
		CustomTableComponent,
		AbpPaginationControlsComponent
	]
})
export class CustomTableModule { }
