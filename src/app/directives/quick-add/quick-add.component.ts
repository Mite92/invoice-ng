import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { QuickAddService } from '@shared/quick-add-service';

@Component({
  selector: 'quick-add',
  templateUrl: './quick-add.component.html',
  styleUrls: ['./quick-add.component.scss']
})
export class QuickAddComponent implements OnInit {

  @Output() onQuickAddClicked = new EventEmitter();
  @Output() onGoToMasterKeyClicked = new EventEmitter();
  quickAddMode: string = "QUICK_ADD";

  masterKeyId: number = this.getMasterKeyId();
  // masterKeyName: string = this.getMasterKeyName();

  constructor(
    private _quickAddService: QuickAddService) { }

  ngOnInit() {
    this.masterKeyId = this.getMasterKeyId();
    // this.masterKeyName = this.getMasterKeyName();
  }


  getMasterKeyId(): number {
    return this._quickAddService.getId();
  }

  getMasterKeyName(): string {
    return this._quickAddService.getName();
  }

}
