import { Directive, ElementRef, OnInit } from '@angular/core';

@Directive({
  selector: '[input-sm]'
})
export class InputSMDirective implements OnInit {

  constructor(private el: ElementRef) { }

  ngOnInit() {

    this.el.nativeElement.classList.add('input-sm')

    if (this.el.nativeElement.classList.contains("disable-input-sm")) {
      this.el.nativeElement.classList.remove("input-sm");
    }
  }

}

  // Usage:
  //     <input type="text" input-sm></input>
  // Creates:
  // 


