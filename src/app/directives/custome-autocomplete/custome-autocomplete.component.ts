import { Component, EventEmitter, Injector, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';
import { MatDialog, MatOptionSelectionChange } from '@angular/material';
import { CreateUpdateProductComponent } from '@app/pages/register/products/create-update-product/create-update-product.component';
import { AppComponentBase } from '@shared/app-component-base';
import { Observable, of, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged } from 'rxjs/operators';
@Component({
	selector: 'custom-autocomplete',
	templateUrl: './custome-autocomplete.component.html',
	styleUrls: ['./custome-autocomplete.component.scss']
})
export class CustomeAutocompleteComponent extends AppComponentBase implements OnInit, OnChanges {

	@Input() listName: string;
	@Input() selectedValue: string;
	@Input() selectedId: number;

	@Input() title: string = "Title";
	@Input() searchService: (filter: string) => Observable<any> = (filter: string): Observable<any> => { return of([]) };
	@Input() getDisplayFn: (item: string) => string = (item: string) => { return "" };
	@Output() onSelectedItem = new EventEmitter<Object>();

	valueChanged: Subject<string> = new Subject<string>();
	searchValue: string;

	options: any = [];


	filteredOptions: Observable<string[]>;
	initialValues: any[] = [];


	constructor(
		private injector: Injector,
		private dialog: MatDialog

	) {
		super(injector)

		this.valueChanged.pipe(
			debounceTime(500),
			distinctUntilChanged()
		).subscribe(x => {
			if (x != "") {
				var items = this.initialValues.filter(option => option.displayName.toLowerCase().includes(x));
				this.options = items;
			} else {
				this.options = this.initialValues;
			}
		});
	}


	ngOnChanges(changes: SimpleChanges): void {
		if (changes && changes.hasOwnProperty("selectedValue") && changes["selectedValue"].currentValue) {
			if (this.searchValue != this.selectedValue)
				this.searchValue = this.selectedValue;
		}
	}

	ngOnInit() {

		this.searchService("").subscribe((result) => {
			this.options = result;
			if (this.initialValues.length == 0)
				this.initialValues = result;
		});
	}

	onSelectionChange(event: MatOptionSelectionChange, item: any) {
		if (event.isUserInput) {
			this.selectedId = item.id;
			this.onSelectedItem.emit(item);
		}
	}

	filterValues() {
		this.valueChanged.next(this.searchValue);
	}

	reset() {
		this.selectedId = null;
		this.selectedValue = "";
		this.searchValue = "";
		if (this.initialValues && this.initialValues.length != 0) {
			this.options = this.initialValues;
		}
	}

	onQuickAddClicked(item: any) {

		const dialogRef = this.dialog.open(CreateUpdateProductComponent, {
			width: '40%',
			data: item
		});

		dialogRef.afterClosed().subscribe(result => {
			if (result) {
				this.searchService("").subscribe((result) => {
					this.options = result;
					this.initialValues = result;
				});
			}
		})
	}

	onContextMenu(event: MouseEvent, keyId: number) {
		this.quickAddService.setId(keyId);
		event.preventDefault();
		this.contextMenuPosition.x = event.clientX + 'px';
		this.contextMenuPosition.y = event.clientY + 'px';
		this.contextMenu.menuData = { 'keyId': keyId };
		this.contextMenu.menu.focusFirstItem('mouse');
		this.contextMenu.openMenu();
	}


}

