import { Directive, OnInit, ElementRef } from '@angular/core';

@Directive({
  /* tslint:disable */
  selector: '[auto-focus]'
})
export class AutoFocusDirective implements OnInit {

  constructor(private el: ElementRef) { }

  ngOnInit(){
    this.el.nativeElement.focus();
  }
}

  // Usage:
  //     <input type="text" auto-focus></input>
  // Creates:
  //
