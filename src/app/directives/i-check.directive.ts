import { Directive, ElementRef, OnInit, Renderer2, HostListener, Output, EventEmitter } from '@angular/core';
import { Component, forwardRef, HostBinding, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

@Directive({
  /* tslint:disable */
  selector: '[i-check]',
  providers: [{
    provide: NG_VALUE_ACCESSOR,
    useExisting: forwardRef(() => ICheckDirective),
    multi: true
  }]
})



export class ICheckDirective implements OnInit, ControlValueAccessor {

  @Input() value: any = true;
  @Input() ngModel: any;
  @Output() change: EventEmitter<any> = new EventEmitter();

  val: any = true;
  onChange = (_: any) => {
  }

  onTouched = () => {
  }


  constructor(private el: ElementRef, public _renderer: Renderer2) { }

  writeValue(value: any): void {
    debugger;
    const normalizedValue = value == null ? "" : value;
    this._renderer.setProperty(this.el.nativeElement, "value", value);
    this._renderer.setAttribute(this.el.nativeElement, "checked", value);
   // $('input').iCheck('checked');
  }

  registerOnChange(fn: (_: any) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onTouched = fn;
  }

  selectElement = $(this.el.nativeElement);







  ngOnInit() {
//     alert(this.value);
//     alert(this.ngModel);


    $('input').on('ifChanged', function(event){

   });

// //this.el.nativeElement.value = true;
//     console.log('vrednost', this.el.nativeElement.value);
// debugger;
//     $('input').iCheck({
//       checkboxClass: 'icheckbox_minimal-blue',
//       radioClass: 'iradio_minimal-blue'
//     });

//     $('input').on('ifChanged', function(event){

//    });

//    if(this.val === true){
//     $('input').iCheck('checked');
//   }else{
//     $('input').iCheck('uncheck');
//   }


  }

}
