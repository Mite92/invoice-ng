import { AfterViewInit, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort } from '@angular/material';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { AppComponentBase } from '@shared/app-component-base';
import { CustomerServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { CreateUpdateCustomerComponent } from './create-update-customer/create-update-customer.component';
import { MatTableDataSource } from '@angular/material/table';

@Component({
  selector: 'app-customers',
  templateUrl: './customers.component.html',
  styleUrls: ['./customers.component.scss'],
  animations: [fadeInRightAnimation, fadeInUpAnimation]

})
export class CustomersComponent extends AppComponentBase implements OnInit, AfterViewInit {

  products: any;
  displayedColumns: string[] = ['id', 'name', 'address', 'city', 'postCode', 'telephone', 'email', 'bankAccount', 'bankName', 'vatNumber', 'actions'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;


  constructor(
    injector: Injector,
    private _customerService: CustomerServiceProxy,
    private dialog: MatDialog
  ) {
    super(injector);
    this.getData();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
  }

  getData() {


    abp.ui.setBusy(
      null,
      this._customerService
        .getAll(999, 0)
        .pipe(
          finalize(() => {
            abp.ui.clearBusy();
          })
        )
        .subscribe((result: any) => {
          this.dataSource.data = result.items;
        })
    );
  }



  create() {
    this.dialog.open(CreateUpdateCustomerComponent, {
      width: '70%',
      height: 'auto',
      data: { mode: "CREATE" },
      disableClose: true
    }).afterClosed().subscribe((entity: any) => {
      this.getData();
    });
  }

  update(item) {
    this.dialog.open(CreateUpdateCustomerComponent, {
      width: '70%',
      height: 'auto',
      data: { mode: "UPDATE", item: item },
      disableClose: true
    }).afterClosed().subscribe((entity: any) => {
      this.getData();
    });
  }

  delete(item) {


    abp.message.confirm(
      "This customer will be deleted",
      "Are you sure?",
      (result: boolean) => {
        if (result) {
          abp.ui.setBusy(
            null,
            this._customerService
              .delete(item.id)
              .pipe(
                finalize(() => {
                  this.openSnackbar("Successfuly deleted")
                  abp.ui.clearBusy();
                })
              )
              .subscribe((result: any) => {
                this.getData();
              })
          );
        }
      }
    );
  }

}
