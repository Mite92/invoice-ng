import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { CustomerServiceProxy } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'app-create-update-customer',
  templateUrl: './create-update-customer.component.html',
  styleUrls: ['./create-update-customer.component.scss']
})
export class CreateUpdateCustomerComponent implements OnInit {


  vatCategories: any;
  model: any = {};

  constructor(
    private _customerService: CustomerServiceProxy,
    private dialog: MatDialog,
    @Optional() @Inject(MAT_DIALOG_DATA) private _data: any
  ) {
  }

  ngOnInit() {
    if (this._data.mode == "UPDATE")
      this.model = this._data.item;
  }


  save() {
    if (this._data.mode == "CREATE") {
      this._customerService.create(this.model).subscribe((result) => {
        this.close();
      });
    } else {
      this._customerService.update(this.model).subscribe((result) => {
        this.close();
      });
    }
  }

  close(): void {
    this.dialog.closeAll();
  }


}
