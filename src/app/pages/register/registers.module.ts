import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ProductsComponent } from './products/products.component';
import { RegistersRoutingModule } from './registers-routing.module';
import { RegisterComponent } from './register.component';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@fury/shared/material-components.module';
import { FurySharedModule } from '@fury/fury-shared.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { SharedModule } from '@shared/shared.module';
import { ListModule } from '@fury/shared/list/list.module';
import { CustomerServiceProxy, MasterKeyServiceProxy, MasterKeyValueServiceProxy, ProductServiceProxy } from '@shared/service-proxies/service-proxies';
import { CustomersComponent } from './customers/customers.component';
import { MasterKeysComponent } from './master-keys/master-keys.component';
import { MasterKeyValuesComponent } from './master-key-values/master-key-values.component';
import { CreateUpdateMasterKeyComponent } from './master-keys/create-update-master-key/create-update-master-key.component';
import { CreateUpdateMasterKeyValuesComponent } from './master-key-values/create-update-master-key-values/create-update-master-key-values.component';
import { CreateUpdateProductComponent } from './products/create-update-product/create-update-product.component';
import { CreateUpdateCustomerComponent } from './customers/create-update-customer/create-update-customer.component';
import { QuickAddService } from '@shared/quick-add-service';



@NgModule({
  declarations: [ProductsComponent, RegisterComponent, CustomersComponent, MasterKeysComponent, MasterKeyValuesComponent, CreateUpdateMasterKeyComponent, CreateUpdateCustomerComponent],
  imports: [
    CommonModule,
    RegistersRoutingModule,
    CommonModule,
    FormsModule,
    MaterialModule,
    FurySharedModule,
    ListModule,
    BreadcrumbsModule,
    SharedModule,
  ],
  providers: [
    ProductServiceProxy,
    CustomerServiceProxy,
    MasterKeyServiceProxy,
    MasterKeyValueServiceProxy,
    QuickAddService
  ],
  entryComponents: [
    CreateUpdateMasterKeyComponent,
    CreateUpdateMasterKeyValuesComponent,
    CreateUpdateProductComponent,
    CreateUpdateCustomerComponent
  ]
})
export class RegistersModule { }
