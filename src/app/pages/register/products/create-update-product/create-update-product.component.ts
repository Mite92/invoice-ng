import { Component, Inject, OnInit, Optional, ViewChild } from '@angular/core';
import { MatDialog, MatDialogRef, MatMenuTrigger, MAT_DIALOG_DATA } from '@angular/material';
import { QuickAddService } from '@shared/quick-add-service';
import { MasterKeyValueServiceProxy, ProductServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateUpdateMasterKeyValuesComponent } from '../../master-key-values/create-update-master-key-values/create-update-master-key-values.component';

@Component({
  selector: 'app-create-update-product',
  templateUrl: './create-update-product.component.html',
  styleUrls: ['./create-update-product.component.scss']
})
export class CreateUpdateProductComponent implements OnInit {

  vatCategories: any;
  model: any = {};
  contextMenuPosition = { x: '0px', y: '0px' };
  @ViewChild(MatMenuTrigger, { static: false })
  contextMenu: MatMenuTrigger;
  vatCategoryMaterKeyId: number;

  constructor(
    private _quickAddService: QuickAddService,
    private _productService: ProductServiceProxy,
    private _keyValuesService: MasterKeyValueServiceProxy,
    private dialogRef: MatDialogRef<CreateUpdateProductComponent>,
    private dialog: MatDialog,
    @Optional() @Inject(MAT_DIALOG_DATA) private _data: any
  ) {
    this.getVatCategories();
  }

  ngOnInit() {
    if (this._data.mode == "UPDATE")
      this.model = this._data.item;
  }



  getVatCategories() {
    this._keyValuesService.getValuesByMasterKeyName("VatCategoryExport").subscribe((result) => {
      this.vatCategories = result;
      if (this.vatCategories.length > 0)
        this.vatCategoryMaterKeyId = result[0].masterKeyId;
    });
  }

  save() {
    if (this._data.mode == "CREATE" || this._data.mode == "QUICK_ADD") {
      this._productService.create(this.model).subscribe((result) => {
        this.close(true);
      });
    } else {
      this._productService.update(this.model).subscribe((result) => {
        this.close(false);
      });
    }
  }

  close(isSaved: boolean): void {
    this.dialogRef.close(isSaved);
  }

  // onContextMenu(event: MouseEvent, keyId: number, name: string) {
  //   this._quickAddService.setId(keyId);
  //   // this._quickAddService.setName(name);
  //   event.preventDefault();
  //   this.contextMenuPosition.x = event.clientX + 'px';
  //   this.contextMenuPosition.y = event.clientY + 'px';
  //   this.contextMenu.menuData = { 'keyId': keyId };
  //   this.contextMenu.menu.focusFirstItem('mouse');
  //   this.contextMenu.openMenu();
  // }

  reRouteToMasterKey(item: any) {
    window.open("/registers/master-key-values");
  }

  onQuickAddClicked(item: any) {
    debugger;

    const dialogRef = this.dialog.open(CreateUpdateMasterKeyValuesComponent, {
      width: '40%',
      data: item
    });

    dialogRef.afterClosed().subscribe(result => {

      if (result) {
      }
    })
  }


}
