import { AfterViewInit, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { AppComponentBase } from '@shared/app-component-base';
import { ProductDto, ProductServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { CreateUpdateProductComponent } from './create-update-product/create-update-product.component';

export interface PeriodicElement {
	name: string;
	position: number;
	weight: number;
	symbol: string;
}

const ELEMENT_DATA: PeriodicElement[] = [
	{ position: 1, name: 'Hydrogen', weight: 1.0079, symbol: 'H' },
	{ position: 2, name: 'Helium', weight: 4.0026, symbol: 'He' },
	{ position: 3, name: 'Lithium', weight: 6.941, symbol: 'Li' },
	{ position: 4, name: 'Beryllium', weight: 9.0122, symbol: 'Be' },
	{ position: 5, name: 'Boron', weight: 10.811, symbol: 'B' },
	{ position: 6, name: 'Carbon', weight: 12.0107, symbol: 'C' },
	{ position: 7, name: 'Nitrogen', weight: 14.0067, symbol: 'N' },
	{ position: 8, name: 'Oxygen', weight: 15.9994, symbol: 'O' },
	{ position: 9, name: 'Fluorine', weight: 18.9984, symbol: 'F' },
	{ position: 10, name: 'Neon', weight: 20.1797, symbol: 'Ne' },
];


@Component({
	selector: 'app-products',
	templateUrl: './products.component.html',
	styleUrls: ['./products.component.scss'],
	animations: [fadeInRightAnimation, fadeInUpAnimation]
})
export class ProductsComponent extends AppComponentBase implements OnInit, AfterViewInit {

	products: any;
	displayedColumns: string[] = ['id', 'displayName', 'um', 'price', 'vatCategoryValue', 'vatCategoryValueNumber', 'actions'];
	dataSource = new MatTableDataSource<any>();

	@ViewChild(MatSort, { static: false }) sort: MatSort;
	@ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

	constructor(
		injector: Injector,
		private _productsService: ProductServiceProxy,
		private dialog: MatDialog
	) {
		super(injector);
		this.getData();
	}

	ngOnInit() {
	}

	ngAfterViewInit() {
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
	}

	getData() {
		abp.ui.setBusy(
			null,
			this._productsService
				.getAll(999, 0)
				.pipe(
					finalize(() => {
						abp.ui.clearBusy();
					})
				)
				.subscribe((result: any) => {
					this.dataSource.data = result.items;
				})
		);
	}


	create() {
		this.dialog.open(CreateUpdateProductComponent, {
			width: '50%',
			height: 'auto',
			data: { mode: "CREATE" },
			disableClose: true
		}).afterClosed().subscribe((entity: any) => {
			this.getData();
		});
	}

	update(item) {
		this.dialog.open(CreateUpdateProductComponent, {
			width: '50%',
			height: 'auto',
			data: { mode: "UPDATE", item: item },
			disableClose: true
		}).afterClosed().subscribe((entity: any) => {
			this.getData();
		});
	}

	delete(item: ProductDto) {
		abp.message.confirm(
			"This product will be deleted",
			"Are you sure?",
			(result: boolean) => {
				if (result) {
					abp.ui.setBusy(
						null,
						this._productsService
							.delete(item.id)
							.pipe(
								finalize(() => {
									this.openSnackbar("Successfuly deleted")
									abp.ui.clearBusy();
								})
							)
							.subscribe((result: any) => {
								this.getData();
							})
					);
				}
			}
		);
	}

}
