import { AfterViewInit, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { AppComponentBase } from '@shared/app-component-base';
import { MasterKeyValueServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { CreateUpdateMasterKeyValuesComponent } from './create-update-master-key-values/create-update-master-key-values.component';

@Component({
  selector: 'app-master-key-values',
  templateUrl: './master-key-values.component.html',
  styleUrls: ['./master-key-values.component.scss'],
  animations: [fadeInRightAnimation, fadeInUpAnimation]

})
export class MasterKeyValuesComponent extends AppComponentBase implements OnInit, AfterViewInit {

  products: any;
  displayedColumns: string[] = ['code', 'masterKeyDisplayName', 'value', 'valueNumber', 'actions'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(
    injector: Injector,
    private _masterKeyValuesService: MasterKeyValueServiceProxy,
    private dialog: MatDialog
  ) {
    super(injector);
    this.getData();
  }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  getData() {


    abp.ui.setBusy(
      null,
      this._masterKeyValuesService
        .getAll(999, 0)
        .pipe(
          finalize(() => {
            abp.ui.clearBusy();
          })
        )
        .subscribe((result: any) => {
          this.dataSource.data = result.items;
        })
    );
  }

  create() {
    this.dialog.open(CreateUpdateMasterKeyValuesComponent, {
      width: 'auto',
      height: 'auto',
      data: { mode: "CREATE" },
      disableClose: true
    }).afterClosed().subscribe((entity: any) => {
      this.getData();
    });
  }

  update(item) {
    this.dialog.open(CreateUpdateMasterKeyValuesComponent, {
      width: 'auto',
      height: 'auto',
      data: { mode: "UPDATE", item: item },
      disableClose: true
    }).afterClosed().subscribe((entity: any) => {
      this.getData();
    });
  }

  delete(item) {


    abp.message.confirm(
      "This item will be deleted",
      "Are you sure?",
      (result: boolean) => {
        if (result) {
          abp.ui.setBusy(
            null,
            this._masterKeyValuesService
              .delete(item.id)
              .pipe(
                finalize(() => {
                  this.openSnackbar("Successfuly deleted")
                  abp.ui.clearBusy();
                })
              )
              .subscribe((result: any) => {
                this.getData();
              })
          );
        }
      }
    );
  }


}
