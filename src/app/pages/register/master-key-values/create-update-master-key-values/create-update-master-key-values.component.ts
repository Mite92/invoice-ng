import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { MasterKeyDto, MasterKeyServiceProxy, MasterKeyValueDto, MasterKeyValueServiceProxy } from '@shared/service-proxies/service-proxies';
import { result } from 'lodash';

@Component({
  selector: 'app-create-update-master-key-values',
  templateUrl: './create-update-master-key-values.component.html',
  styleUrls: ['./create-update-master-key-values.component.scss']
})
export class CreateUpdateMasterKeyValuesComponent implements OnInit {

  model: any = {};
  masterKeys: MasterKeyDto[] = [];
  endOfYearContos: MasterKeyValueDto[];

  constructor(
    private dialogRef: MatDialogRef<CreateUpdateMasterKeyValuesComponent>,
    private _masterKeysService: MasterKeyServiceProxy,
    private _masterKeyValuesService: MasterKeyValueServiceProxy,
    @Optional() @Inject(MAT_DIALOG_DATA) private _data: any
  ) {
    this.getMasterKeys();
    this.getEndOfYearContos();
  }

  ngOnInit() {
    if (this._data.mode == "UPDATE")
      this.model = this._data.item;

    debugger;
    if (this._data.mode == "QUICK_ADD")
      this.model.masterKeyId = this._data.masterKeyId;
  }

  save() {
    if (this._data.mode == "CREATE" || this._data.mode == "QUICK_ADD") {
      this._masterKeyValuesService.create(this.model).subscribe((result) => {
        this.close(true);
      });
    } else {
      this._masterKeyValuesService.update(this.model).subscribe((result) => {
        this.close(false);
      });
    }
  }

  close(isSaved: boolean): void {
    this.dialogRef.close(isSaved);

  }

  getEndOfYearContos() {
    this._masterKeyValuesService.getMasterKeyValuesByListName(["EndOfYearContos"]).subscribe((result) => {
      this.endOfYearContos = result;

    });
  }

  getMasterKeys() {
    this._masterKeysService.getAll(999, 0).subscribe((result) => {
      this.masterKeys = result.items;
    })
  }

}
