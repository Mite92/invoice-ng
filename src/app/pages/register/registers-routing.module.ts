import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { RegisterComponent } from './register.component';
import { ProductsComponent } from './products/products.component';
import { CustomersComponent } from './customers/customers.component';
import { MasterKeysComponent } from './master-keys/master-keys.component';
import { MasterKeyValuesComponent } from './master-key-values/master-key-values.component';



const routes: Routes = [
  {
    path: '', component: RegisterComponent, children: [
      { path: 'products', component: ProductsComponent },
      { path: 'customers', component: CustomersComponent },
      { path: 'master-keys', component: MasterKeysComponent },
      { path: 'master-key-values', component: MasterKeyValuesComponent }
    ]
  }
];


@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class RegistersRoutingModule { }
