import { Component, Inject, OnInit, Optional } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { MasterKeyServiceProxy } from '@shared/service-proxies/service-proxies';
import { result } from 'lodash';

@Component({
    selector: 'app-create-update-master-key',
    templateUrl: './create-update-master-key.component.html',
    styleUrls: ['./create-update-master-key.component.scss']
})
export class CreateUpdateMasterKeyComponent implements OnInit {

    model: any = {};
    constructor(
        private dialog: MatDialog,
        private _masterKeysService: MasterKeyServiceProxy,
        @Optional() @Inject(MAT_DIALOG_DATA) private _data: any
    ) { }

    ngOnInit() {
        if (this._data.mode == "UPDATE")
            this.model = this._data.item;
    }

    save() {
        if (this._data.mode == "CREATE") {
            this._masterKeysService.create(this.model).subscribe((result) => {
                this.close();
            });
        } else {
            this._masterKeysService.update(this.model).subscribe((result) => {
                this.close();
            });
        }
    }

    close(): void {
        this.dialog.closeAll();
    }

}
