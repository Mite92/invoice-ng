import { Component, Input, OnInit } from '@angular/core';
import { InvoiceStatisticsDto } from '@shared/service-proxies/service-proxies';

@Component({
  selector: 'fury-quick-info-widget',
  templateUrl: './quick-info-widget.component.html',
  styleUrls: ['./quick-info-widget.component.scss']
})
export class QuickInfoWidgetComponent implements OnInit {

  @Input() value: string;
  @Input() label: string;
  @Input() background: string;
  @Input() color: string;
  @Input() data: InvoiceStatisticsDto;


  @Input() icon: string;

  constructor() {
  }

  ngOnInit() {
    console.log(this.data)
  }

}
