import { Component, Inject, Injector, OnInit, Optional, ViewChild } from '@angular/core';
import { FormControl } from '@angular/forms';
import { MatDialog, MatMenuTrigger, MatOptionSelectionChange, MAT_DIALOG_DATA } from '@angular/material';
import { CreateUpdateMasterKeyValuesComponent } from '@app/pages/register/master-key-values/create-update-master-key-values/create-update-master-key-values.component';
import { AppComponentBase } from '@shared/app-component-base';
import { QuickAddService } from '@shared/quick-add-service';
import { CustomerServiceProxy, MasterKeyValueDto, MasterKeyValueServiceProxy, PurchaseServiceProxy } from '@shared/service-proxies/service-proxies';
import { result } from 'lodash';
import { Observable, Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, map, startWith } from 'rxjs/operators';

@Component({
  selector: 'app-create-update-purchase',
  templateUrl: './create-update-purchase.component.html',
  styleUrls: ['./create-update-purchase.component.scss']
})
export class CreateUpdatePurchaseComponent extends AppComponentBase implements OnInit {
  customers: any;
  contos: MasterKeyValueDto[] = [];
  vatCategories: any[] = [];
  model: any = {};

  vatCategoryMaterKeyId: number;
  selectedCategory: any;

  myControl = new FormControl();
  options: any[] = [];
  filteredOptions: Observable<string[]>;

  destinationCountryChanged: Subject<string> = new Subject<string>();
  destinationCities: MasterKeyValueDto;
  destinationCountries: MasterKeyValueDto[];
  categoriesForType: any[] = [];


  // private _filter(value: string): string[] {
  //   const filterValue = value.toLowerCase();
  //   debugger;
  //   return this.options.filter(option => option.value.toLowerCase().includes(filterValue));
  // }

  onSelectedForeignCurrencyPrefix(item: MasterKeyValueDto) {
    this.model.vatCategoryId = item.id;
  }

  foreignCurrencyDisplayFn(item: MasterKeyValueDto) {
    debugger;
    return item.valueNumber;
  }

  foreignCurrencySearch(filter: any) {

    return this._keyValuesService.get(filter).pipe(
      map(x => {
        return x
      })
    )
  }



  // async onDestinationCountrySelectionChange(event: MatOptionSelectionChange, item: any) {
  //   if (event.isUserInput) {

  //     this._keyValuesService.get(event.source.value).subscribe(x => {
  //       this.destinationCities = x;
  //     })
  //   }
  // // }
  // async filterDestinationCountries(val: any) {
  //   await this.destinationCountryChanged.next(val);
  // }


  constructor(
    injector: Injector,
    private _customersService: CustomerServiceProxy,
    private _keyValuesService: MasterKeyValueServiceProxy,
    private _purchaseService: PurchaseServiceProxy,
    private dialog: MatDialog,
    @Optional() @Inject(MAT_DIALOG_DATA) private _data: any
  ) {
    super(injector);
    this.getCutomers();
    this.getVatCategories();

    this.foreignCurrencySearch = this.foreignCurrencySearch.bind(this);
  }

  ngOnInit() {
    if (this._data.mode == "UPDATE")
      this.model = this._data.item;
  }

  getCutomers() {
    this._customersService.getAll(999, 0).subscribe((result) => {
      this.customers = result.items;
    });
  }

  //VatCategoryImport, Expenses
  getVatCategories() {
    this._keyValuesService.getMasterKeyValuesByListName(["VatCategoryImport", "EXPENSES"]).subscribe((result) => {
      this.vatCategories = result;
      this.categoriesForType = this.vatCategories;
      this.options = result;
      if (this.vatCategories.length > 0)
        this.vatCategoryMaterKeyId = result[0].masterKeyId;
    });
  }

  // getContos() {
  //   this._keyValuesService.getValuesByMasterKeyName("ProfitAndLoss").subscribe((result) => {
  //     this.contos = result;
  //   });
  // }

  save() {
    if (this._data.mode == "CREATE") {
      this._purchaseService.create(this.model).subscribe((result) => {
        this.close();
      });
    } else {
      this._purchaseService.update(this.model).subscribe((result) => {
        this.close();
      });
    }
  }

  close(): void {
    this.dialog.closeAll();
  }

  calculateVAT() {
    if (this.selectedCategory != null) {
      this.model.totalVat = ((this.model.totalInclVat * (this.selectedCategory.valueNumber / (1 + (this.selectedCategory.valueNumber / 100)))) / 100);
      this.model.totalExclVat = this.model.totalInclVat - this.model.totalVat;

      this.model.totalVat = Number(this.model.totalVat.toFixed(2));
      this.model.totalExclVat = Number(this.model.totalExclVat.toFixed(2));
    }

  }

  typeChange(type) {
    debugger;
    this.categoriesForType = this.vatCategories.filter(x => x.description == type);

  }

  categoryChange(categoryId) {
    this.selectedCategory = this.categoriesForType.find(x => x.id == categoryId);
  }

  calculateInclVatAndVat() {

    if (this.selectedCategory != null) {
      this.model.totalVat = (this.model.totalExclVat * this.selectedCategory.valueNumber) / 100;
      this.model.totalInclVat = this.model.totalExclVat + this.model.totalVat;
      this.model.totalVat = Number(this.model.totalVat.toFixed(2));
      this.model.totalInclVat = Number(this.model.totalInclVat.toFixed(2));
    }

  }



  onQuickAddClicked(item: any) {

    const dialogRef = this.dialog.open(CreateUpdateMasterKeyValuesComponent, {
      width: '40%',
      data: item
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.getVatCategories();
      }
    })
  }

}
