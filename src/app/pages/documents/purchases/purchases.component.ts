import { AfterViewInit, Component, Injector, OnInit, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { AppComponentBase } from '@shared/app-component-base';
import { PurchaseDto, PurchaseServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { CreateUpdatePurchaseComponent } from './create-update-purchase/create-update-purchase.component';

@Component({
  selector: 'app-purchases',
  templateUrl: './purchases.component.html',
  styleUrls: ['./purchases.component.scss'],
  animations: [fadeInRightAnimation, fadeInUpAnimation]

})
export class PurchasesComponent extends AppComponentBase implements OnInit, AfterViewInit {

  products: any;
  displayedColumns: string[] = ['purchaseNumber', 'supplierName', 'invoiceNumber', 'dateFormatted', 'dateOfPaymentFormatted', 'vatCategoryValue', 'vatCategoryValueNumber', 'totalVat', 'totalExclVat', 'totalInclVat', 'actions'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;

  constructor(
    injector: Injector,
    private _purchasesService: PurchaseServiceProxy,
    private dialog: MatDialog
  ) {
    super(injector);
    this.getData();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
  }

  getData() {
    abp.ui.setBusy(
      null,
      this._purchasesService
        .getAll(999, 0)
        .pipe(
          finalize(() => {
            abp.ui.clearBusy();
          })
        )
        .subscribe((result: any) => {
          this.dataSource.data = result.items;
        })
    );
  }

  create() {
    this.dialog.open(CreateUpdatePurchaseComponent, {
      width: '60%',
      height: 'auto',
      data: { mode: "CREATE" },
      disableClose: true
    }).afterClosed().subscribe((entity: any) => {
      this.getData();
    });
  }

  update(item: PurchaseDto) {
    this.dialog.open(CreateUpdatePurchaseComponent, {
      width: '60%',
      height: 'auto',
      data: { mode: "UPDATE", item: item },
      disableClose: true
    }).afterClosed().subscribe((entity: any) => {
      this.getData();
    });
  }

  delete(item: PurchaseDto) {
    abp.message.confirm(
      "This purchase will be deleted",
      "Are you sure?",
      (result: boolean) => {
        if (result) {
          abp.ui.setBusy(
            null,
            this._purchasesService
              .delete(item.id)
              .pipe(
                finalize(() => {
                  this.openSnackbar("Successfuly deleted");
                  abp.ui.clearBusy();
                })
              )
              .subscribe((result: any) => {
                this.getData();

              })
          );
        }
      }
    );
  }

}
