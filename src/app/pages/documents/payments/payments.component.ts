import { Component, Inject, Injector, OnInit, Optional } from '@angular/core';
import { MatDialogRef, MatTableDataSource, MAT_DIALOG_DATA } from '@angular/material';
import { AppComponentBase } from '@shared/app-component-base';
import { PaymentsDto, PaymentsServiceProxy } from '@shared/service-proxies/service-proxies';
import { forEach, result } from 'lodash';
import * as moment from 'moment';
import { finalize } from 'rxjs/operators';


@Component({
	selector: 'app-payments',
	templateUrl: './payments.component.html',
	styleUrls: ['./payments.component.scss']
})
export class PaymentsComponent extends AppComponentBase implements OnInit {

	model: PaymentsDto = new PaymentsDto();
	invoice: any = {};
	amountPaid: number = 0;
	amountDue: number = 0;



	constructor(
		injector: Injector,
		private dialogRef: MatDialogRef<PaymentsComponent>,
		private _paymentsService: PaymentsServiceProxy,
		@Optional() @Inject(MAT_DIALOG_DATA) private _data: any
	) {
		super(injector)
	}

	displayedColumns: string[] = ['id', 'paymentDateFormatted', 'amount', 'actions'];
	dataSource = new MatTableDataSource<any>();

	ngOnInit() {

		debugger;

		if (this._data.mode == 'ADD_PAYMENT') {
			this.model.invoiceId = this._data.item.invoice.id;
			this.invoice = this._data.item.invoice;
		} else {
			this.model.invoiceId = this._data.item.id;
			this.invoice = this._data.item;
		}

		this.getData();

		this.dialogRef.keydownEvents().subscribe(x => {
			if (x.key === "Escape") this.dialogRef.close();
		});
	}

	save() {

		if (this.model.amount != null || this.model.paymentDate) {
			this.model.paymentDate = moment(this.model.paymentDate).add(2, 'hours');
			this._paymentsService.create(this.model).subscribe((result) => {
				this.openSnackbar("Payment added successfuly");
				this.getData();
			});
		}

	}

	getData() {
		abp.ui.setBusy(
			null,
			this._paymentsService
				.getAllByInvoiceId(this.model.invoiceId)
				.pipe(
					finalize(() => {
						abp.ui.clearBusy();
					})
				)
				.subscribe((result: any) => {
					this.dataSource.data = result.items;
					this.calculateAmountPaid();
				})
		);
	}

	calculateAmountPaid() {
		this.amountPaid = 0;
		this.dataSource.data.forEach(item => {
			this.amountPaid += item.amount;
		});

		this.amountDue = Number((this.invoice.total - this.amountPaid).toFixed(2));
	}

	delete(item) {
		this._paymentsService.delete(item.id).subscribe((result) => {
			this.getData();
		})
	}



}
