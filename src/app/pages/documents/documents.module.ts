import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InvoicesComponent } from './invoices/invoices.component';
import { PurchasesComponent } from './purchases/purchases.component';
import { DocumentsRoutingModule } from './documents-routing.module';
import { FormsModule } from '@angular/forms';
import { MaterialModule } from '@fury/shared/material-components.module';
import { FurySharedModule } from '@fury/fury-shared.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { ListModule } from '@fury/shared/list/list.module';
import { SharedModule } from '@shared/shared.module';
import { DocumentsComponent } from './documents.component';
import { CustomerServiceProxy, InvoiceServiceProxy, MasterKeyServiceProxy, MasterKeyValueServiceProxy, PaymentsServiceProxy, ProductServiceProxy, PurchaseServiceProxy } from '@shared/service-proxies/service-proxies';
import { CreateUpdatePurchaseComponent } from './purchases/create-update-purchase/create-update-purchase.component';
import { CreateUpdateInvoiceComponent } from './invoices/create-update-invoice/create-update-invoice.component';
import { QuickAddService } from '@shared/quick-add-service';
import { CreateUpdateMasterKeyValuesComponent } from '../register/master-key-values/create-update-master-key-values/create-update-master-key-values.component';
import { CreateUpdateProductComponent } from '../register/products/create-update-product/create-update-product.component';
import { CustomeAutocompleteComponent } from '@app/directives/custome-autocomplete/custome-autocomplete.component';
import { PaymentsComponent } from './payments/payments.component';
import { PaymentsListComponent } from './payments-list/payments-list.component';



@NgModule({

  declarations: [CustomeAutocompleteComponent, InvoicesComponent, PurchasesComponent, DocumentsComponent, CreateUpdatePurchaseComponent, CreateUpdateInvoiceComponent, PaymentsComponent, PaymentsListComponent],
  imports: [
    CommonModule,
    DocumentsRoutingModule,
    CommonModule,
    FormsModule,
    MaterialModule,
    FurySharedModule,
    ListModule,
    BreadcrumbsModule,
    SharedModule,
  ],
  providers: [
    PurchaseServiceProxy,
    InvoiceServiceProxy,
    MasterKeyValueServiceProxy,
    CustomerServiceProxy,
    ProductServiceProxy,
    QuickAddService,
    MasterKeyServiceProxy,
    ProductServiceProxy,
    PaymentsServiceProxy
  ],
  entryComponents: [
    CreateUpdatePurchaseComponent,
    CreateUpdateInvoiceComponent,
    CreateUpdateMasterKeyValuesComponent,
    CreateUpdateProductComponent,
    PaymentsComponent
  ]
})
export class DocumentsModule { }
