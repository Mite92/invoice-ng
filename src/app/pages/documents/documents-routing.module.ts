import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { DocumentsComponent } from './documents.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { PurchasesComponent } from './purchases/purchases.component';
import { PaymentsListComponent } from './payments-list/payments-list.component';

const routes: Routes = [
  {
    path: '', component: DocumentsComponent, children: [
      { path: 'invoices', component: InvoicesComponent },
      { path: 'purchases', component: PurchasesComponent },
      { path: 'payments', component: PaymentsListComponent }
    ]
  }
];

@NgModule({
  declarations: [],
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DocumentsRoutingModule { }
