import { Component, Inject, Injector, OnInit, Optional, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { AppComponentBase } from '@shared/app-component-base';
import { InvoiceServiceProxy, PaymentsServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { PaymentsComponent } from '../payments/payments.component';

@Component({
  selector: 'app-payments-list',
  templateUrl: './payments-list.component.html',
  styleUrls: ['./payments-list.component.scss']
})
export class PaymentsListComponent extends AppComponentBase implements OnInit {

  products: any;
  displayedColumns: string[] = ['invoiceNumber', 'invoiceDate', 'invoiceCustomer', 'invoiceTotal', 'paid', 'balance', 'isPaid', 'actions'];
  dataSource = new MatTableDataSource<any>();

  @ViewChild(MatSort, { static: false }) sort: MatSort;
  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;


  constructor(
    private injector: Injector,
    private _invoiceServicel: InvoiceServiceProxy,
    private dialog: MatDialog
  ) {
    super(injector);

    this.getData();
  }

  ngAfterViewInit() {
    this.dataSource.sort = this.sort;
    this.dataSource.paginator = this.paginator;
  }

  ngOnInit() {
  }

  getData() {
    abp.ui.setBusy(
      null,
      this._invoiceServicel
        .getInvoicePaymentStatus()
        .pipe(
          finalize(() => {
            abp.ui.clearBusy();
          })
        )
        .subscribe((result: any) => {
          this.dataSource.data = result.items;
        })
    );
  }

  addPayment(item) {
    this.dialog.open(PaymentsComponent, {
      width: '60%',
      // height: '95%',
      // maxHeight: '95%',
      data: { mode: "ADD_PAYMENT", item: item },
      disableClose: true
    }).afterClosed().subscribe((entity: any) => {
    });
  }
}
