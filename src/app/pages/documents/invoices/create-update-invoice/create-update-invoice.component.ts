import { Component, Inject, Injector, OnInit, Optional } from '@angular/core';
import { MatDialog, MAT_DIALOG_DATA } from '@angular/material';
import { CreateUpdateProductComponent } from '@app/pages/register/products/create-update-product/create-update-product.component';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { AppComponentBase } from '@shared/app-component-base';
import { CustomerDto, CustomerServiceProxy, InvoiceServiceProxy, LnkInvoiceProductDto, ProductDto, ProductServiceProxy } from '@shared/service-proxies/service-proxies';
import * as _ from 'lodash';
import * as moment from 'moment';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';


@Component({
  selector: 'app-create-update-invoice',
  templateUrl: './create-update-invoice.component.html',
  styleUrls: ['./create-update-invoice.component.scss'],
  animations: [fadeInRightAnimation, fadeInUpAnimation]
})
export class CreateUpdateInvoiceComponent extends AppComponentBase implements OnInit {

  customers: CustomerDto[] = [];
  products: ProductDto[] = [];
  model: any = {};
  totalVat: number = 0;
  totalExclVat: number = 0;
  totalInclVat: number = 0;

  constructor(
    injector: Injector,
    private _customersService: CustomerServiceProxy,
    private _productService: ProductServiceProxy,
    private _invoiceServicel: InvoiceServiceProxy,
    private dialog: MatDialog,
    @Optional() @Inject(MAT_DIALOG_DATA) private _data: any
  ) {
    super(injector);
    this.getCutomers();
    this.getProducts();
    this.substitutionSearch = this.substitutionSearch.bind(this);

  }

  ngOnInit() {
    if (this._data.mode == "UPDATE") {
      this.model = this._data.item;
    } else {
      this.model.invoiceDate = new Date(this.model.invoiceDate);
      this.model.invoiceNumber = this._data.id + 1 + "/" + moment().year();
    }

    if (this.model.lnkInvoiceProduct && this.model.lnkInvoiceProduct.length > 0)
      this.calculateInvoiceTotals();
  }

  getCutomers() {
    this._customersService.getAll(999, 0).subscribe((result) => {
      this.customers = result.items;
    });
  }

  getProducts() {
    this._productService.getAll(999, 0).subscribe((result) => {
      this.products = result.items;
    });
  }

  addProduct() {
    if (this.model.lnkInvoiceProduct == undefined) {
      this.model.lnkInvoiceProduct = [];
    }
    this.model.lnkInvoiceProduct.push(new LnkInvoiceProductDto());
  }

  deleteProduct(product) {



    this.model.lnkInvoiceProduct = _.filter(this.model.lnkInvoiceProduct, (o) => {
      return o != product;
    });

    this.calculateInvoiceTotals();
  }

  close(): void {
    this.dialog.closeAll();
  }


  onSelectedItem(event, item) {
    item.productPrice = event.price
    item.quantity = 0;
    item.productVatCategoryValueNumber = event.vatCategoryValueNumber;
    item.productId = event.id;
    this.quantityChange(item);
  }


  productChange(productId: number, item: any) {

    let selected = this.products.find(x => x.id == productId);

    if (selected != null) {
      item.productPrice = selected.price
      item.quantity = 0;
      item.productVatCategoryValueNumber = selected.vatCategoryValueNumber;
      item.productId = selected.id;
      this.quantityChange(item);
    }

  }

  discountChange() {
    this.totalInclVat = this.totalInclVat - this.model.discount;
  }

  quantityChange(item) {
    item.tax = Number((((item.productVatCategoryValueNumber / 100) * item.productPrice) * item.quantity).toFixed(2));
    item.totalExclVat = Number((item.productPrice * item.quantity).toFixed(2));
    item.total = Number(((item.totalExclVat + item.tax)).toFixed(2));
    this.calculateInvoiceTotals();
  }

  save() {
    this.model.totalVatAmount = this.totalVat;
    this.model.subTotal = this.totalExclVat;
    this.model.total = this.totalInclVat;

    this.model.invoiceDate = moment(this.model.invoiceDate).add(2, 'hours');

    if (this._data.mode == "CREATE") {
      this._invoiceServicel.create(this.model).subscribe((result) => {
        this.close();
      });
    } else {
      this._invoiceServicel.update(this.model).subscribe((result) => {
        this.close();
      });
    }
  }

  calculateInvoiceTotals() {

    this.totalVat = 0;
    this.totalExclVat = 0;
    this.totalInclVat = 0;

    this.model.lnkInvoiceProduct.forEach(el => {
      el.totalExclVat = el.productPrice * el.quantity;
      this.totalVat += el.tax;
      this.totalExclVat += el.totalExclVat;
      this.totalInclVat += el.total
    });

    this.totalVat = Number(this.totalVat.toFixed(2));
    this.totalExclVat = Number(this.totalExclVat.toFixed(2));
    this.totalInclVat = Number(this.totalInclVat.toFixed(2));

  }


  onQuickAddClicked(item: any) {

    const dialogRef = this.dialog.open(CreateUpdateProductComponent, {
      width: '40%',
      data: item
    });

    dialogRef.afterClosed().subscribe(result => {
      debugger;
      if (result) {
        this.getProducts();
      }
    })
  }

  substitutionSearch(filter: string): Observable<ProductDto[]> {
    let input: ProductDto = new ProductDto();
    input.displayName = filter;
    return this._productService
      .search(input)
      .pipe(map(x => {
        return x.items;
      }));
  }

  substitutionDisplayFn(item: ProductDto) {
    return item.displayName;
  }

}
