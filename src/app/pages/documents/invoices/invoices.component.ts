import { AfterViewInit, Component, Inject, Injector, OnInit, Optional, ViewChild } from '@angular/core';
import { MatDialog, MatPaginator, MatSort, MatTableDataSource } from '@angular/material';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { AppComponentBase } from '@shared/app-component-base';
import { API_BASE_URL, InvoiceServiceProxy } from '@shared/service-proxies/service-proxies';
import { finalize } from 'rxjs/operators';
import { PaymentsComponent } from '../payments/payments.component';
import { CreateUpdateInvoiceComponent } from './create-update-invoice/create-update-invoice.component';

@Component({
	selector: 'app-invoices',
	templateUrl: './invoices.component.html',
	styleUrls: ['./invoices.component.scss'],
	animations: [fadeInRightAnimation, fadeInUpAnimation]
})
export class InvoicesComponent extends AppComponentBase implements OnInit, AfterViewInit {

	products: any;
	displayedColumns: string[] = ['invoiceNumber', 'invoiceDateFormatted', 'customerName', 'totalVatAmount', 'subTotal', 'total', 'discount', 'actions'];
	dataSource = new MatTableDataSource<any>();

	@ViewChild(MatSort, { static: false }) sort: MatSort;
	@ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
	baseURL: string = "";


	constructor(
		private injector: Injector,
		private _invoiceService: InvoiceServiceProxy,
		private dialog: MatDialog,
		@Optional() @Inject(API_BASE_URL) baseURL?: string
	) {
		super(injector);
		this.baseURL = baseURL ? baseURL : "";

		this.getData();
	}

	ngAfterViewInit() {
		this.dataSource.sort = this.sort;
		this.dataSource.paginator = this.paginator;
	}

	ngOnInit() {
	}

	getData() {
		abp.ui.setBusy(
			null,
			this._invoiceService
				.getAll(999, 0)
				.pipe(
					finalize(() => {
						abp.ui.clearBusy();
					})
				)
				.subscribe((result: any) => {
					this.dataSource.data = result.items;

				})
		);


	}

	create() {
		let lastId = this.dataSource.data[0].id;


		this.dialog.open(CreateUpdateInvoiceComponent, {
			width: '90%',
			// height: '95%',
			maxHeight: '95%',
			data: { mode: "CREATE", id: lastId },
			disableClose: true
		}).afterClosed().subscribe((entity: any) => {
			this.getData();
		});
	}

	update(item) {
		this.dialog.open(CreateUpdateInvoiceComponent, {
			width: '90%',
			// height: '95%',
			maxHeight: '95%',
			data: { mode: "UPDATE", item: item },
			disableClose: true
		}).afterClosed().subscribe((entity: any) => {
			this.getData();
		});
	}

	delete(item) {
		abp.message.confirm(
			"This invoice will be deleted",
			"Are you sure?",
			(result: boolean) => {
				if (result) {
					abp.ui.setBusy(
						null,
						this._invoiceService
							.delete(item.id)
							.pipe(
								finalize(() => {
									this.openSnackbar("Successfuly deleted")
									abp.ui.clearBusy();
								})
							)
							.subscribe((result: any) => {
								this.getData();

							})
					);
				}
			}
		);
	}

	print(item) {
		window.open(
			this.baseURL + "/api/PrintPreview/Print?id=" + item.id);
	}

	addPayment(item) {
		this.dialog.open(PaymentsComponent, {
			width: '60%',
			// height: '95%',
			// maxHeight: '95%',
			data: { mode: "UPDATE", item: item },
			disableClose: true
		}).afterClosed().subscribe((entity: any) => {
		});
	}

}
