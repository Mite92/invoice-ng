import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UserResetPasswordComponent } from './user-reset-password.component';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    SharedModule
  ],
  declarations: [UserResetPasswordComponent],
  entryComponents: [UserResetPasswordComponent],
  exports: [UserResetPasswordComponent]
})
export class UserResetPasswordModule {
}
