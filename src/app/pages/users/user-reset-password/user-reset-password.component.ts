import { Component, Inject, OnInit, Injector, Optional } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import {  UserServiceProxy, ResetPasswordDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'fury-user-reset-password',
  templateUrl: './user-reset-password.component.html',
  styleUrls: ['./user-reset-password.component.scss']
})
export class UserResetPasswordComponent extends AppComponentBase
 implements OnInit {

  public isLoading = false;
  public resetPasswordDto: ResetPasswordDto;
  
  constructor(
    injector: Injector,
    private _userService: UserServiceProxy,
    private _dialogRef: MatDialogRef<UserResetPasswordComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _userId: number,
    private snackbar: MatSnackBar
  ) {
    super(injector);
  }

  ngOnInit() {
    this.isLoading = true;
    this.resetPasswordDto = new ResetPasswordDto();
    this.resetPasswordDto.userId = this._userId;
    this.resetPasswordDto.newPassword = Math.random()
      .toString(36)
      .substr(2, 10);
    this.isLoading = false;
  }

  public resetPassword(): void {
    this.isLoading = true;
    this._userService
      .resetPassword(this.resetPasswordDto)
      .pipe(
        finalize(() => {
          this.isLoading = false;
        })
      )
      .subscribe(() => {
        // this.notify.info('Password Reset');
        this.snackbar.open('Password Reset', 'OK', {
          duration: 10000
        });
        this.close(true);
      });
  }

  close(result: any): void {
    this._dialogRef.close(result);
  }
}
