import { AfterViewInit, Component, Input, OnDestroy, OnInit, ViewChild, Injector } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { finalize } from 'rxjs/operators';
import { fadeInRightAnimation } from '@fury/animations/fade-in-right.animation';
import { fadeInUpAnimation } from '@fury/animations/fade-in-up.animation';
import { ListColumn } from '@fury/shared/list/list-column.model';
import {
  PagedListingComponentBase,
  PagedRequestDto
} from '@shared/paged-listing-component-base';
import {
  UserServiceProxy,
  UserDto,
  UserDtoPagedResultDto
} from '@shared/service-proxies/service-proxies';
import { UserCreateUpdateComponent } from './user-create-update/user-create-update.component';
import { UserResetPasswordComponent } from './user-reset-password/user-reset-password.component';


class PagedUsersRequestDto extends PagedRequestDto {
  keyword: string;
  isActive: boolean | null;
}


@Component({
  selector: 'fury-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss'],
  animations: [fadeInRightAnimation, fadeInUpAnimation]
})
export class UsersComponent extends PagedListingComponentBase<UserDto> {

  users: UserDto[] = [];
  keyword = '';
  isActive: boolean | null;

  constructor(
    injector: Injector,
    private _userService: UserServiceProxy,
    private _dialog: MatDialog
  ) {
    super(injector);
  }

  list(
    request: PagedUsersRequestDto,
    pageNumber: number,
    finishedCallback: Function
  ): void {
    request.keyword = this.keyword;
    request.isActive = this.isActive;
    request.maxResultCount = 99;

    this.dataSource = new MatTableDataSource();

    this._userService
      .getAll(request.keyword, request.isActive, request.skipCount, request.maxResultCount)
      .pipe(
        finalize(() => {
          finishedCallback();
        })
      )
      .subscribe((result: UserDtoPagedResultDto) => {
        this.users = result.items;
        this.showPaging(result, pageNumber);
        this.dataSource.data = this.users;
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }

  delete(user: UserDto): void {
    abp.message.confirm(
      this.l('UserDeleteWarningMessage', user.fullName),
      this.l('AreYouSure'),
      (result: boolean) => {
        if (result) {
          this._userService
            .delete(user.id)
            .pipe(
              finalize(() => {
                abp.notify.success(this.l('SuccessfullyDeleted'));
                this.refresh();
              })
            )
            .subscribe(() => { });
        }
      }
    );


  }

  createUser() {
    this._dialog.open(UserCreateUpdateComponent, {
      width: '600px',
    }).afterClosed().subscribe((user: UserDto) => {
      if (user) {
        this.refresh();
      }
    });
  }

  updateUser(user) {
    this._dialog.open(UserCreateUpdateComponent, {
      // height: '450px',
      width: '600px',
      data: user.id
    }).afterClosed().subscribe((user) => {
      if (user) {
        this.refresh();
      }
    });
  }

  public resetPassword(user: UserDto): void {
    console.log(user.id);
    this.showResetPasswordUserDialog(user.id);
  }

  private showResetPasswordUserDialog(userId?: number): void {
    this._dialog.open(UserResetPasswordComponent, {
      // height: '450px',
      width: '600px',
      data: userId
    });
  }


  @Input()
  columns: ListColumn[] = [
    // { name: 'Checkbox', property: 'checkbox', visible: false },

    { name: this.l('UserName'), property: 'userName', visible: true, isModelProperty: true },
    { name: this.l('FullName'), property: 'fullName', visible: true, isModelProperty: true },
    { name: this.l('EmailAddress'), property: 'emailAddress', visible: true, isModelProperty: true },
    { name: 'Active', property: 'isActive', visible: true },

    { name: 'Actions', property: 'actions', visible: true }
  ] as ListColumn[];
  pageSize = 10;
  dataSource: MatTableDataSource<UserDto> | null;

  @ViewChild(MatPaginator, { static: false }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  get visibleColumns() {
    return this.columns.filter(column => column.visible).map(column => column.property);
  }

  ngAfterViewInit() {
    this.dataSource.paginator = this.paginator;
    this.dataSource.sort = this.sort;
  }

  // onFilterChange(value) {
  //   if (!this.dataSource) {
  //     return;
  //   }
  //   value = value.trim();
  //   value = value.toLowerCase();
  //   this.dataSource.filter = value;
  // }

  onFilterSubmit(value) {
    this.keyword = value;
    this.refresh();
  }


}
