import { Component, Inject, OnInit, Injector, Optional } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { UserDto, UserServiceProxy, CreateUserDto, RoleDto } from '@shared/service-proxies/service-proxies';
import { AppComponentBase } from '@shared/app-component-base';
import { MatCheckboxChange } from '@angular/material';
import * as _ from 'lodash';
import { finalize } from 'rxjs/operators';

@Component({
  selector: 'fury-user-create-update',
  templateUrl: './user-create-update.component.html',
  styleUrls: ['./user-create-update.component.scss']
})
export class UserCreateUpdateComponent extends AppComponentBase
 implements OnInit {

  saving = false;
  user: UserDto = new UserDto();
  roles: RoleDto[] = [];
  checkedRolesMap: { [key: string]: boolean } = {};
  defaultRoleCheckedStatus = false;

  inputType = 'password';
  visible = false;

  form: FormGroup;
  mode: 'create' | 'update' = 'create';

  constructor(
    injector: Injector,
    private _userService: UserServiceProxy,
    private dialogRef: MatDialogRef<UserCreateUpdateComponent>,
    @Optional() @Inject(MAT_DIALOG_DATA) private _id: number,
    private fb: FormBuilder
  ) {
    super(injector);
  }

  ngOnInit() {
    if (this._id) {
      this.mode = 'update';
    } else {
      this.mode = 'create';
    }

    if (this.mode == 'update') {
      this._userService.get(this._id).subscribe(result => {
        this.user = result;

        this._userService.getRoles().subscribe(result2 => {
          this.roles = result2.items;
          this.setInitialRolesStatus();
        });
      });
      this.form = this.fb.group({
        userName: '',
        name: '',
        surname: '',
        emailAddress: '',
        isActive: ''
      });
    } else {
      this.user.isActive = true;

      this._userService.getRoles().subscribe(result => {
        this.roles = result.items;
        this.setInitialRolesStatus();
      });
      this.form = this.fb.group({
        userName: '',
        name: '',
        surname: '',
        emailAddress: '',
        password: '',
        confirmPassword: '',
        isActive: ''
      });
    }
  }

  setInitialRolesStatus(): void {
    _.map(this.roles, item => {
      this.checkedRolesMap[item.normalizedName] = this.isRoleChecked(
        item.normalizedName
      );
    });
  }

  isRoleChecked(normalizedName: string): boolean {
    if (this.isCreateMode())
    // just return default role checked status
    // it's better to use a setting
      return this.defaultRoleCheckedStatus;
    else
      return _.includes(this.user.roleNames, normalizedName);
  }

  onRoleChange(role: RoleDto, $event: MatCheckboxChange) {
    this.checkedRolesMap[role.normalizedName] = $event.checked;
  }

  getCheckedRoles(): string[] {
    const roles: string[] = [];
    _.forEach(this.checkedRolesMap, function (value, key) {
      if (value) {
        roles.push(key);
      }
    });
    return roles;
  }

  save() {
    this.saving = true;

    this.user.roleNames = this.getCheckedRoles();

    if (this.mode === 'create') {
      this.createUser();
    } else if (this.mode === 'update') {
      this.updateUser();
    }
  }

  createUser() {
    const user_ = new CreateUserDto();
    user_.init(this.user);

    this._userService
      .create(user_)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  updateUser() {
    this._userService
      .update(this.user)
      .pipe(
        finalize(() => {
          this.saving = false;
        })
      )
      .subscribe(() => {
        this.notify.info(this.l('SavedSuccessfully'));
        this.close(true);
      });
  }

  isCreateMode() {
    return this.mode === 'create';
  }

  isUpdateMode() {
    return this.mode === 'update';
  }

  togglePassword() {
    if (this.visible) {
      this.inputType = 'password';
      this.visible = false;
    } else {
      this.inputType = 'text';
      this.visible = true;
    }
  }

  close(result: any): void {
    this.dialogRef.close(result);
  }
}
