import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { UsersRoutingModule } from './users-routing.module';
import { UsersComponent } from './users.component';
import { UserCreateUpdateModule } from './user-create-update/user-create-update.module';
import { UserResetPasswordModule } from './user-reset-password/user-reset-password.module';
import { UserServiceProxy } from '@shared/service-proxies/service-proxies';
import { FurySharedModule } from '@fury/fury-shared.module';
import { MaterialModule } from '@fury/shared/material-components.module';
import { ListModule } from '@fury/shared/list/list.module';
import { BreadcrumbsModule } from '@fury/shared/breadcrumbs/breadcrumbs.module';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    UsersRoutingModule,
    FormsModule,
    MaterialModule,
    FurySharedModule,

    // Core
    ListModule,
    UserCreateUpdateModule,
    UserResetPasswordModule,
    BreadcrumbsModule,
    SharedModule,
  ],
  providers: [
    UserServiceProxy
  ],
  declarations: [UsersComponent],
  exports: [UsersComponent]
})
export class UsersModule {
}
