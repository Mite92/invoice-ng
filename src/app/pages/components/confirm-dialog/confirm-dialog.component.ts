import { Component, Inject, OnInit } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';

@Component({
  selector: 'app-confirm-dialog',
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  title: string;
  subTitle: string;

  constructor(@Inject(MAT_DIALOG_DATA) private options: any,
    private dialogRef: MatDialogRef<ConfirmDialogComponent>) {
    this.dialogRef.updateSize("500px");
  }

  ngOnInit() {
    this.title = this.options.title;
    this.subTitle = this.options.subTitle;
  }

  cancel() {
    this.dialogRef.close(false);
  }

  confirm() {
    this.dialogRef.close(true);
  }

}
