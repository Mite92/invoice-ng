import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AppSessionService } from '@shared/session/app-session.service';

@Component({
  selector: 'fury-toolbar-user',
  templateUrl: './toolbar-user.component.html',
  styleUrls: ['./toolbar-user.component.scss']
})
export class ToolbarUserComponent implements OnInit {

  isOpen: boolean;
  
  @Output() openConfig = new EventEmitter();
  @Output() logout = new EventEmitter();

  constructor(public _sessionService: AppSessionService) { }

  ngOnInit() {
  }

  toggleDropdown() {
    this.isOpen = !this.isOpen;
  }

  onClickOutside() {
    this.isOpen = false;
  }

}
