import { Component, EventEmitter, Input, OnInit, Output, Injector } from '@angular/core';
import { SidenavItem } from '../../sidenav/sidenav-item/sidenav-item.interface';
import { SidenavService } from '@app/layout/sidenav/sidenav.service';
import { AppComponentBase } from '@shared/app-component-base';

@Component({
  selector: 'fury-navigation-item',
  templateUrl: './navigation-item.component.html',
  styleUrls: ['./navigation-item.component.scss']
})
export class NavigationItemComponent extends AppComponentBase implements OnInit {

  @Input('item') item: SidenavItem;
  @Input('currentlyOpen') currentlyOpen: SidenavItem[] = [];

  @Output() handleClick = new EventEmitter<SidenavItem>();

  constructor(
    injector: Injector,
  ) { 
    super(injector);
  }

  ngOnInit() {
  }

  showMenuItem(item = this.item): boolean {
    if (item.permission) {
      return this.permission.isGranted(item.permission);
    }

    return true;
  }

  anySubMenu(): boolean {
    if (this.item.subItems) {
      for (let item of this.item.subItems)
        if (this.showMenuItem(item))
          return true;

      return false;
    }
    return true;
  }

}
