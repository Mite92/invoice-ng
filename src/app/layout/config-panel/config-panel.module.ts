import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ConfigPanelComponent } from './config-panel.component';
import { FurySharedModule } from '../../../@fury/fury-shared.module';
import { ConfigPanelToggleComponent } from './config-panel-toggle/config-panel-toggle.component';
import { ConfigurationServiceProxy } from '@shared/service-proxies/service-proxies';
import { LanguageSwitchComponent } from '../languageswitch/languageswitch.component';
import { SharedModule } from '@shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    FurySharedModule,
    SharedModule
  ],
  declarations: [
    ConfigPanelComponent,
    ConfigPanelToggleComponent,
    LanguageSwitchComponent
  ],
  providers: [
    ConfigurationServiceProxy
  ],
  exports: [
    ConfigPanelComponent,
    ConfigPanelToggleComponent,
    LanguageSwitchComponent
  ]
})
export class ConfigPanelModule {
}
