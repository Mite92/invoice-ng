// "Production" enabled environment

export const environment = {
    production: true,
    hmr: false,
    appConfig: 'appconfig.production.json',
    googleMapsApiKey: '',
    backend: 'http://localhost:4200' // Put your backend here
};
