import { atinaTemplatePage } from './app.po';

describe('atina App', function() {
  let page: atinaTemplatePage;

  beforeEach(() => {
    page = new atinaTemplatePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
